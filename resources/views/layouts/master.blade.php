<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="{{$masterName}} - {{$masterDesc}}">

    <!-- share -->
    @yield('sharing-meta')

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
	<title>
		{{$masterName}} | @yield('title')
	</title>

	<script type="text/javascript">
		window.Laravel = {!! json_encode([
		    'csrfToken' => csrf_token(),
		]); !!}
	</script>

	<link rel="shortcut icon" href="{{asset($storage_url.'img/'.$masterFavicon->setting_value)}}?{{$masterFavicon->setting_desc}}" type="image/x-icon" />
	<link rel="stylesheet" type="text/css" href="{{ asset($asset_url.'css/app.css') }}">

  @php
    $x = array_count_values($webFont);
  @endphp

  @foreach ($x as $key => $value)
    @if ($key != "")
      <link href="//fonts.googleapis.com/css?family={{ $key }}" rel="stylesheet" type="text/css">
    @endif
  @endforeach

  @include('layouts.masterCSS')
</head>
<body>
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbarCollapse" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="{{ url('/') }}">
					<img class="img" alt="logo" src="{{ !empty($masterLogo) ? asset($storage_url.'img/'.$masterLogo) : 'http://via.placeholder.com/200x200' }}">
					<span class="text hidden-xs custom-navbar-header">
						{{ $masterName }}
					</span>
				</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="navbarCollapse">

				<!--<form class="navbar-form navbar-right hidden-xs" method="get" action="">
					<div class="form-group">
						<input type="text" name="q" class="form-control" placeholder="Search" required="">
					</div>
					<button type="submit" class="btn btn-default">
						<i class="fa fa-search" aria-hidden="true"></i>
					</button>
				</form>-->

				<ul class="nav navbar-nav navbar-right">
          @if($publishPage[0] == "publish")
					   <li><a class="hover-line {{ ($page == 'section1')?'active':'' }}" href="{{ route('section1') }}">{{ $pageName[0] }}</a></li>
          @endif
          @if($publishPage[1] == "publish")
					   <li><a class="hover-line {{ ($page == 'section2')?'active':'' }}" href="{{ route('section2') }}">{{ $pageName[1] }}</a></li>
          @endif
					<li><a class="hover-line {{ ($page == 'about')?'active':'' }}" href="{{ route('about') }}">{{ $pageName[2] }}</a></li>
					<li><a class="hover-line {{ ($page == 'contact')?'active':'' }}" href="{{ route('contact') }}">{{ $pageName[3] }}</a></li>
					{{-- <li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="#">Action</a></li>
								<li><a href="#">Another action</a></li>
								<li><a href="#">Something else here</a></li>
								<li role="separator" class="divider"></li>
								<li><a href="#">Separated link</a></li>
								<li role="separator" class="divider"></li>
								<li><a href="#">One more separated link</a></li>
							</ul>
					</li> --}}
				</ul>

				<!--<form class="navbar-form navbar-right visible-xs" method="get" action="">
					<div class="form-group">
						<input type="text" name="q" class="form-control" placeholder="Search" required="">
					</div>
					<button type="submit" class="btn btn-default">
						<i class="fa fa-search" aria-hidden="true"></i>
					</button>
				</form>-->

			</div>
		</div>
	</nav>

	@yield('content')

	<footer class="main-footer">
		<div class="container footer-top">
			<div class="row">
				<div class="col-xs-12 col-sm-4 navigation">
					<h4>Navigasi</h4>
					<ul>
            @if($publishPage[0] == "publish")
						   <li><a href="{{ route('section1') }}">{{ $pageName[0] }}</a></li>
            @endif
            @if($publishPage[1] == "publish")
						   <li><a href="{{ route('section2') }}">{{ $pageName[1] }}</a></li>
            @endif
						<li><a href="{{ route('about') }}">{{ $pageName[2] }}</a></li>
						<li><a href="{{ route('contact') }}">{{ $pageName[3] }}</a></li>
					</ul>
				</div>
				<div class="col-xs-12 col-sm-4 contact">
					<h4>Kontak</h4>
					<ul>
						<li>{{ $masterName }}</li>
						<li>{{ $masterAddress}}</li>
						@if($masterPhone != "")
						<li>
              @php
								$phone_numbers = explode(';',$masterPhone);
							@endphp
							@foreach ($phone_numbers as $key => $value)
								@php
	                preg_match_all('!\d+!', $value, $x);
	                $call = implode($x[0]);
	              @endphp
								<a href="tel:{{ $call }}">{{ $value }}</a><br>
							@endforeach
						</li>
						@endif
						@if($masterEmail != "")
						<li><a href="mailto:{{ $masterEmail }}">{{ $masterEmail }}</a></li>
						@endif
					</ul>
				</div>
				<div class="col-xs-12 col-sm-4 social-media">
					@php
						$fb = $masterFb;
						$ig = $masterIg;
						$tw = $masterTw;
            $pt = $masterPt;
            $pa = $masterPa;
            $gp = $masterGp;
            $li = $masterLi;
            $sc = $masterSc;
					@endphp
					@if($fb != "" || $ig != "" || $tw != "" || $pt != "" || $pa != "" || $gp != "" || $li != ""|| $sc != "")
					<h4>Media Sosial</h4>
					@endif
          <div class="col-xs-12">
					@if($fb != "")
					<a href="{{ $masterFb }}" class="facebook" target="_blank">
						<i class="fa fa-facebook-official" aria-hidden="true"></i>
					</a>
					@endif
					@if($ig != "")
					<a href="{{ $masterIg }}" class="instagram" target="_blank">
						<i class="fa fa-instagram" aria-hidden="true"></i>
					</a>
					@endif
					@if($tw != "")
					<a href="{{ $masterTw }}" class="twitter" target="_blank">
						<i class="fa fa-twitter-square" aria-hidden="true"></i>
					</a>
					@endif
          @if($pt != "")
					<a href="{{ $masterPt }}" class="pinterest" target="_blank">
						<i class="fa fa-pinterest" aria-hidden="true"></i>
					</a>
					@endif
        </div>
        <div class="col-xs-12 socmed-bottom">
          @if($pa != "")
					<a href="{{ $masterPa }}" class="path" target="_blank">
						<i class="fa fa-pinterest-square" aria-hidden="true"></i>
					</a>
					@endif
          @if($gp != "")
					<a href="{{ $masterGp }}" class="googleplus" target="_blank">
						<i class="fa fa-google-plus-official" aria-hidden="true"></i>
					</a>
					@endif
          @if($li != "")
					<a href="{{ $masterLi }}" class="linkedin" target="_blank">
						<i class="fa fa-linkedin-square" aria-hidden="true"></i>
					</a>
					@endif
          @if($sc != "")
					<a href="{{ $masterSc }}" class="snapchat" target="_blank">
						<i class="fa fa-snapchat-square" aria-hidden="true"></i>
					</a>
					@endif
          </div>
				</div>
			</div>
		</div>
		<div class="container-fluid footer-bottom">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-6 bottom-left">
						&copy; {{ $masterName." ".date('Y')}} - All rights reserved
					</div>
					<div class="col-xs-12 col-sm-6 bottom-right">
						A concept by <a href="http://www.ptcerise.com/" target="_blank">Cerise</a>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<script type="text/javascript" src="{{ asset($asset_url.'js/app.js') }}"></script>
</body>
</html>
