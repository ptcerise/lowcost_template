@php
  function colorBrightness($hex, $percent) {
    // Work out if hash given
    $hash = '';
    if (stristr($hex,'#')) {
      $hex = str_replace('#','',$hex);
      $hash = '#';
    }
    /// HEX TO RGB
    $rgb = array(hexdec(substr($hex,0,2)), hexdec(substr($hex,2,2)), hexdec(substr($hex,4,2)));
    //// CALCULATE
    for ($i=0; $i<3; $i++) {
      // See if brighter or darker
      if ($percent > 0) {
        // Lighter
        $rgb[$i] = round($rgb[$i] * $percent) + round(255 * (1-$percent));
      } else {
        // Darker
        $positivePercent = $percent - ($percent*2);
        $rgb[$i] = round($rgb[$i] * $positivePercent) + round(0 * (1-$positivePercent));
      }
      // In case rounding up causes us to go to 256
      if ($rgb[$i] > 255) {
        $rgb[$i] = 255;
      }
    }
    //// RBG to Hex
    $hex = '';
    for($i=0; $i < 3; $i++) {
      // Convert the decimal digit to hex
      $hexDigit = dechex($rgb[$i]);
      // Add a leading zero if necessary
      if(strlen($hexDigit) == 1) {
      $hexDigit = "0" . $hexDigit;
      }
      // Append to the hex string
      $hex .= $hexDigit;
    }
    return $hash.$hex;
  }
@endphp
<style>

  /* GLOBAL FONT */
  @if (!empty($webFont[0]))
    @php
      $x = explode(':', $webFont[0]);
      $x = preg_replace('/[^a-zA-Z0-9]+/', ' ', $x[0]);
      $fontfamily = $x;
    @endphp
    html,body{
      font-family: '{{ $fontfamily }}';
    }
  @endif
  /* NAVBAR FONT */
  @if (!empty($webFont[1]))
    @php
      $x = explode(':', $webFont[1]);
      $x = preg_replace('/[^a-zA-Z0-9]+/', ' ', $x[0]);
      $fontfamily = $x;
    @endphp
    .navbar .navbar-nav > li > a{
      font-family: '{{ $fontfamily }}';
    }
  @endif

  /* CONTENT FONT */
  @if (!empty($webFont[2]))
    @php
      $x = explode(':', $webFont[2]);
      $x = preg_replace('/[^a-zA-Z0-9]+/', ' ', $x[0]);
      $fontfamily = $x;
    @endphp
    .home-content p,
    .divider p,
    .section-content .content-box.content-text,
    .section-content .content-box.contact-info .contact-detail p,
    .section-content .content-box.contact-info .contact-detail a,
    footer.main-footer .footer-top .navigation ul > li > a,
    footer.main-footer .footer-top .contact ul,
    footer.main-footer .footer-bottom{
      font-family: '{{ $fontfamily }}';
    }
  @endif

  /* HEADING FONT */
  @if (!empty($webFont[3]))
    @php
      $x = explode(':', $webFont[3]);
      $x = preg_replace('/[^a-zA-Z0-9]+/', ' ', $x[0]);
      $fontfamily = $x;
    @endphp
    .home-content h2,
    .home-content .card .card-content h3,
    .section-content .content-box.content-text h2,
    .section-content .content-box.contact-info #contactForm label,
    .section-content .content-box.contact-info .contact-detail h2,
    footer.main-footer .footer-top h4{
      font-family: '{{ $fontfamily }}';
    }
  @endif

  /* IMAGE CAPTION FONT */
  @if (!empty($webFont[4]))
    @php
      $x = explode(':', $webFont[4]);
      $x = preg_replace('/[^a-zA-Z0-9]+/', ' ', $x[0]);
      $fontfamily = $x;
    @endphp
    .banner-img .banner-content .content-text h2,
    .carousel .carousel-text,
    header .banner-section .banner-section-content h1{
      font-family: '{{ $fontfamily }}';
    }
  @endif

  /* IMAGE SUB-CAPTION FONT */
  @if (!empty($webFont[5]))
    @php
      $x = explode(':', $webFont[5]);
      $x = preg_replace('/[^a-zA-Z0-9]+/', ' ', $x[0]);
      $fontfamily = $x;
    @endphp
    .banner-img .banner-content .content-text p{
      font-family: '{{ $fontfamily }}';
    }
  @endif

  /* COMPANY NAME FONT */
  @if (!empty($webFont[6]))
    @php
      $x = explode(':', $webFont[6]);
      $x = preg_replace('/[^a-zA-Z0-9]+/', ' ', $x[0]);
      $fontfamily = $x;
    @endphp
    .navbar .navbar-header .navbar-brand span.text,
    .section-content .content-box.contact-info .contact-detail h2{
      font-family: '{{ $fontfamily }}';
    }
  @endif

  /* BUTTON FONT */
  @if (!empty($webFont[7]))
    @php
      $x = explode(':', $webFont[7]);
      $x = preg_replace('/[^a-zA-Z0-9]+/', ' ', $x[0]);
      $fontfamily = $x;
    @endphp
    .btn-default{
      font-family: '{{ $fontfamily }}';
    }
  @endif

  .navbar-default,
  .divider,
  .card,
  footer.main-footer{
    background: #{{$navbarColor[0]}};
    background: -webkit-linear-gradient(#{{$navbarColor[0]}}, #{{$navbarColor[1]}});
    background: -o-linear-gradient(#{{$navbarColor[0]}}, #{{$navbarColor[1]}});
    background: -moz-linear-gradient(#{{$navbarColor[0]}}, #{{$navbarColor[1]}});
    background: linear-gradient(#{{$navbarColor[0]}}, #{{$navbarColor[1]}});
  }

  footer.main-footer .footer-bottom {
    background-color: {{colorBrightness('#'.$navbarColor[0],-0.9)}};
    border-top-color: {{colorBrightness('#'.$navbarColor[0],-0.9)}};
  }

  footer.main-footer .footer-top,
  footer.main-footer .footer-top .contact ul > li > a,
  footer.main-footer .footer-top .navigation ul > li > a,
  footer.main-footer .footer-top .social-media a,
  footer.main-footer .footer-bottom .bottom-right a{
    color: #{{$webColor[3]}};
  }

  footer.main-footer .footer-top .navigation ul > li > a:hover{
    color: #{{$webColor[0]}};
  }

  footer.main-footer .footer-top .contact ul > li > a:hover,
  footer.main-footer .footer-bottom .bottom-right a:hover{
    color: {{colorBrightness('#'.$webColor[3],0.8)}};
  }

  .navbar .navbar-header .navbar-brand span.text,
  footer.main-footer .footer-top h4,
  a,
  .navbar .navbar-nav li > a.hover-line.active,
  .navbar .navbar-nav > li > a:hover{
    color: #{{$webColor[0]}};
  }

  .navbar .navbar-nav > li > a.hover-line:hover:after{
    background: #{{$webColor[0]}};
  }

  .navbar .navbar-nav > li > a{
    color: #{{$webColor[3]}};
  }

  a:hover{
    color: {{colorBrightness('#'.$webColor[0],0.8)}};
  }

  .btn-default,
  button.navbar-toggle,
  .carousel .slideDown > button{
    background-color: #{{$btnColor[0]}};
    color: #{{$btnColor[1]}};
  }

  .btn-default:hover,
  .carousel .slideDown > button:hover{
    background-color: #{{$btnColor[2]}};
    color: #{{$btnColor[3]}};
  }

  body{
    background-color: #{{$webColor[1]}};
    color: #{{$webColor[3]}};
  }

  .banner-img .banner-content .content-text,
  header .banner-section .banner-section-content h1,
  .carousel .carousel-text{
    color: #{{$txtBannerColor}};
  }

  .carousel-indicators li{
    border-color: #{{$txtBannerColor}};
  }

  .carousel-indicators .active{
    background-color: #{{$txtBannerColor}}};
  }

  .section-content .content-box.left{
    border-color: #{{$webColor[3]}};
  }

  .section-content .content-box.contact-info .contact-detail a{
    color: #{{$webColor[3]}};
  }

  .section-content .content-box.contact-info .contact-detail a:hover{
    color: {{colorBrightness('#'.$webColor[3],0.8)}};
  }

  .page404{
    color: #{{$webColor[0]}};
  }

  .page404 h1{
    font-size: 200px;
    font-weight: bold;
  }

  .page404 h1 i{
    position: relative;
    top: 25px;
  }

  .page404 p{
    font-size: 24px;
  }

  @media screen and (max-width: 768px){

    .page404{
      margin-top: 100px;
    }

    .page404 h1{
      font-size: 120px;
      font-weight: bold;
    }

    .page404 h1 i{
      position: relative;
      top: 15px;
    }
  }
</style>
