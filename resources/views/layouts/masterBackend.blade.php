<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $masterName }} | @yield('title')</title>

    <link rel="shortcut icon" href="{{asset($storage_url.'img/'.$masterFavicon->setting_value)}}?{{$masterFavicon->setting_desc}}" type="image/x-icon" />
    <!-- Bootstrap Core CSS -->
    <link href="{{asset($asset_url.'css/backend/bootstrap.min.css')}}" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="{{asset($asset_url.'css/backend/metisMenu.min.css')}}" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="{{asset($asset_url.'css/backend/timeline.css')}}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{asset($asset_url.'css/backend/startmin.css')}}" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="{{asset($asset_url.'css/backend/morris.css')}}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <link href="//cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">

    <link href="{{asset($asset_url.'css/backend/chosen.css')}}" rel="stylesheet">
    <link href="{{asset($asset_url.'css/backend/Flat.css')}}" rel="stylesheet">
    <link href="{{asset($asset_url.'css/backend/ImageSelect.css')}}" rel="stylesheet">
    <link href="{{asset($asset_url.'css/backend/fontselect.css')}}" rel="stylesheet">
    <link href="{{ asset($asset_url.'css/app_backend.css') }}" rel="stylesheet">
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">{{ $masterName }}</a>
        </div>

        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>

        <!-- Top Navigation: Left Menu -->
        <ul class="nav navbar-nav navbar-left navbar-top-links">
            <li><a href="{{ url('/') }}" target="_blank"><i class="fa fa-home fa-fw"></i> Website</a></li>
        </ul>

        <!-- Top Navigation: Right Menu -->
        <ul class="nav navbar-right navbar-top-links">
            <li class="dropdown navbar-inverse">
                <a href="{{ url('/backend/contact#messages') }}">
                    @php
                        $msg = App\Message::where(['message_status'=>'1'])->get();
                    @endphp
                    <i class="fa fa-envelope fa-fw"></i> <b>{{ count($msg) }}</b>
                </a>
                {{-- <ul class="dropdown-menu dropdown-alerts">
                    <li>
                        <a href="#">
                            <div>
                                <i class="fa fa-comment fa-fw"></i> New Comment
                                <span class="pull-right text-muted small">4 minutes ago</span>
                            </div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a class="text-center" href="#">
                            <strong>See All Alerts</strong>
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </li>
                </ul> --}}
            </li>
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-user fa-fw"></i> {{ Auth::user()->name }} <b class="caret"></b>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    {{-- <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                    </li> --}}
                    <li>
                        <a
                            href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();"
                        >
                            <i class="fa fa-sign-out fa-fw"></i> Logout
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </li>
        </ul>

        <!-- Sidebar -->
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">

                <ul class="nav" id="side-menu">
                    {{-- <li class="sidebar-search">
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button class="btn btn-primary" type="button">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                        </div>
                    </li> --}}
                    {{-- <li>
                        <a href="{{ url('backend/dashboard') }}" class="{{($page == 'dashboard')?'active':''}}"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                    </li> --}}
                    <li>
                        <a href="{{ url('backend/home') }}" class=""><i class="fa fa-home fa-fw"></i> Beranda</a>
                    </li>
                    <li>
                        <a href="#" class="{{($page == 'sections' || $page == 'section1' || $page == 'section2')?'active':''}}"><i class="fa fa-newspaper-o fa-fw"></i> Halaman
                        <span class="fa arrow"></a>
                        <ul class="nav nav-second-level collapse in">
                            <li>
                                <a class="{{($page == 'section1')?'active':''}}" href="{{ url('/backend/sections/section1') }}">
                                    <i class="fa fa-file-text-o fa-fw"></i> {{ ucwords($pageName[0]) }}
                                    {{-- <span id="section1stat" class="pull-right" title="{{($publishPage[0] == "publish")?'This page is published':'This page is not published'}}"><i class="fa {{($publishPage[0] == "publish")?'fa-eye':'fa-eye-slash'}}" aria-hidden="true"></i></span> --}}
                                </a>
                            </li>
                            <li>
                                <a class="{{($page == 'section2')?'active':''}}" href="{{ url('/backend/sections/section2') }}">
                                    <i class="fa fa-file-text-o fa-fw"></i> {{ ucwords($pageName[1]) }} <span id="section2stat" class="pull-right" title="{{($publishPage[1] == "publish")?'This page is published':'This page is not published'}}"><i class="fa {{($publishPage[1] == "publish")?'fa-eye':'fa-eye-slash'}}" aria-hidden="true"></i></span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="{{ url('backend/about') }}" class=""><i class="fa fa-question-circle fa-fw"></i> {{ ucwords($pageName[2]) }}</a>
                    </li>
                    <li>
                        <a href="{{ url('backend/contact') }}" class=""><i class="fa fa-phone fa-fw"></i> {{ ucwords($pageName[3]) }}</a>
                    </li>
                    <li>
                        <a href="{{ url('backend/settings') }}" class=""><i class="fa fa-gear fa-fw"></i> Settings</a>
                    </li>
                    @php
                        if(Auth::user()->level == '1'){
                    @endphp
                        <li>
                            <a href="{{ url('backend/user_management') }}" class="{{($page == 'users')?'active':''}}"><i class="fa fa-users fa-fw"></i> Manajemen Pengguna</a>
                        </li>
                    @php
                        }
                    @endphp
                    {{-- <li>
                        <a href="#"><i class="fa fa-sitemap fa-fw"></i> Multi-Level Dropdown<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="#">Second Level Item</a>
                            </li>
                            <li>
                                <a href="#">Third Level <span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="#">Third Level Item</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li> --}}
                </ul>

            </div>
        </div>
    </nav>

    <!-- Page Content -->
    <main id="page-wrapper">

        <div class="container-fluid">

            <header>
                <div class="col-lg-12">
                    <h1 class="page-header">
                      @yield('title')
                      @if($page == "section2")
                        @php
                          if($page == "section1"):
                            $publishStat = $publishPage[0];
                          else:
                            $publishStat = $publishPage[1];
                          endif;
                        @endphp
                        <button id="publishBtn" class="btn btn-warning pull-right" title="{{($publishStat == "publish")?'This page is published':'This page is not published'}}" data-page="{{$page}}" data-publish="{{$publishStat}}">
                          <i class="fa {{($publishStat == "publish")?'fa-eye':'fa-eye-slash'}}" aria-hidden="true"></i>
                        </button>
                      @endif
                    </h1>
                </div>
            </header>

            <!-- ... Your content goes here ... -->
            @yield('content')

        </div>
    </div>

</div>

<!-- jQuery -->
<script src="{{asset($asset_url.'js/backend/jquery.min.js')}}"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{{asset($asset_url.'js/backend/bootstrap.min.js')}}"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="{{asset($asset_url.'js/backend/metisMenu.min.js')}}"></script>

<!-- Custom Theme JavaScript -->
<script src="{{asset($asset_url.'js/backend/startmin.js')}}"></script>

<script src="{{asset($asset_url.'js/backend/jquery.fontselect.js')}}"></script>

<script type="text/javascript" src="//cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>

<script src="//cloud.tinymce.com/stable/tinymce.min.js?apiKey=db4v2bd2xf5bvaqx5vav7jzsv0hb3h0nmbiytqdmlinnt28b"></script>

<script src="{{ asset($asset_url.'js/backend/chosen.jquery.min.js') }}"></script>
<script src="{{ asset($asset_url.'js/backend/ImageSelect.jquery.js') }}"></script>

<script src="{{ asset($asset_url.'js/app_backend.js') }}"></script>

@if(Session::has('anchor'))
    <script type="text/javascript">
        window.location.href="{{ Session::get('anchor') }}";
    </script>
@endif

@yield('script')
{{-- <script src="{{asset(mix('js/app.js'))}}"></script> --}}

</body>
</html>
