<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="{{$masterName}} - {{$masterDesc}}">

    <!-- share -->
    @yield('sharing-meta')

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
	<title>
		{{$masterName}} | {{ ucfirst('Error 404 Not Found') }}
	</title>

	<script type="text/javascript">
		window.Laravel = {!! json_encode([
		    'csrfToken' => csrf_token(),
		]); !!}
	</script>

	<link rel="shortcut icon" href="{{asset($storage_url.'img/'.$masterFavicon->setting_value)}}?{{$masterFavicon->setting_desc}}" type="image/x-icon" />
	<link rel="stylesheet" type="text/css" href="{{ asset($asset_url.'css/app.css') }}">
  @include('layouts.masterCSS')
</head>
<body>

<main class="container-fluid text-center page404">

  <h1>4<i class="fa fa-frown-o" aria-hidden="true"></i>4</h1>
  <div class="col-xs-12 col-sm-4 col-sm-offset-4" style="margin-top: 20px;padding-top:20px;border-top: 1px solid #{{$webColor[0]}};">
    <p>Page Not Found</p>
  </div>
</main>
<footer class="main-footer" style="position: fixed;bottom:0;width:100%;">
  <div class="container text-center">
      <button class="btn btn-default" style="margin-bottom: 20px" onclick="window.history.back();">Back</button>
  </div>
  <div class="container-fluid footer-bottom">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-6 bottom-left">
          &copy; {{ $masterName." ".date('Y')}} - All rights reserved
        </div>
        <div class="col-xs-12 col-sm-6 bottom-right">
          A concept by <a href="http://www.ptcerise.com/" target="_blank">Cerise</a>
        </div>
      </div>
    </div>
  </div>
</footer>
<script type="text/javascript" src="{{ asset($asset_url.'js/app.js') }}"></script>
</body>
</html>
