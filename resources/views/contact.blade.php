@extends('layouts.master')

@php
	$main_title = $pageName[3];
@endphp
@section('title')
	{{ ucfirst($main_title) }}
@endsection

@section('sharing-meta')
@php
	$share_url = Request::url();
	$share_title = $masterName." | ".ucfirst($main_title);
	$share_desc = $masterName." | ".$masterAddress." | ".$masterPhone." | ".$masterEmail;
	$share_img = asset($storage_url.'img/contact/'.$banner);
@endphp
{{-- FACEBOOK --}}
<meta property="og:url" content="{{$share_url}}" />
<meta property="og:type" content="place" />
<meta property="og:title" content="{{$share_title}}" />
<meta property="og:description" content="{{ $share_desc }}" />
<meta property="og:image" content="{{$share_img}}" />

{{-- TWITTER --}}
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="{{$share_url}}">
<meta name="twitter:title" content="{{$share_title}}">
<meta name="twitter:description" content="{{ $share_desc }}">
<meta name="twitter:image:src" content="{{$share_img}}">

{{--GOOGLE PLUS --}}
<meta itemprop="name" content="{{ $share_title }}">
<meta itemprop="description" content="{{ $share_desc }}">
<meta itemprop="image" content="{{$share_img}}">

@endsection

@section('content')

	<header class="container-fluid text-center">
		@php
			$url = asset($storage_url.'img/contact/'.$banner);
		@endphp
		<div class="banner-section">
			<img alt="banner-header" src="{{ $url }}" />
			<div class="banner-section-content">
				<h1 data-animate="true" data-effect="flipInX">
					{{ $main_title }}
				</h1>
			</div>
		</div>
	</header>

	<!-- MAIN CONTENT -->
	<main id="mainContent" class="container-fluid">
		<section class="row section-content">
			<div class="col-xs-12 col-sm-6 content-box left contact-info">
				<div class="row">
					<div class="col-xs-12">
						<div class="row" data-animate="true" data-effect="fadeInLeft">
							<div class="col-xs-12 col-sm-3 text-center">
								@php
									$url = asset($storage_url.'img/'.$masterLogo);
								@endphp
								<img class="img img-responsive contact-logo" alt="logo" src="{{ $url }}">
							</div>
							<div class="col-xs-12 col-sm-9 contact-detail">
								<h2>{{ $masterName }}</h2>
								<p>{{ $masterAddress }}</p>
								@php
									$phone_numbers = explode(';',$masterPhone);
								@endphp
								@foreach ($phone_numbers as $key => $value)
									@php
		                preg_match_all('!\d+!', $value, $x);
		                $call = implode($x[0]);
		              @endphp
									<a href="tel:{{ $call }}">{{ $value }}</a><br>
								@endforeach
							</div>
						</div>
					</div>
					<div class="col-xs-12" data-animate="true" data-effect="fadeInUp">

						<form id="contactForm" class="" method="post" action="{{ url('/contact/send_message') }}">

							@if(Session::has('message'))
								<div class="alert {{ Session::get('alert-class') }} alert-dismissible" role="alert">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								 	<p>{{ Session::get('message') }}</p>
								</div>
							@endif

							{{csrf_field()}}
							<div class="row form-group">
								<div class="col-xs-12 col-sm-3">
									<label class="control-label" for="name">
										Nama
									</label>
								</div>
								<div class="col-xs-12 col-sm-9{{ $errors->has('name') ? ' has-error' : '' }}">
									<input class="form-control" type="text" name="name" value="{{old('name')}}">
									@if ($errors->has('name'))
                      <span class="help-block">
                          <strong>{{ $errors->first('name') }}</strong>
                      </span>
                  @endif
								</div>
							</div>
							<div class="row form-group">
								<div class="col-xs-12 col-sm-3">
									<label class="control-label" for="email">
										Email
									</label>
								</div>
								<div class="col-xs-12 col-sm-9{{ $errors->has('email') ? ' has-error' : '' }}">
									<input class="form-control" type="email" name="email" value="{{old('email')}}">
									@if ($errors->has('email'))
                      <span class="help-block">
                          <strong>{{ $errors->first('email') }}</strong>
                      </span>
                  @endif
								</div>
							</div>
							<div class="row form-group">
								<div class="col-xs-12 col-sm-9 col-sm-offset-3{{ $errors->has('message') ? ' has-error' : '' }}">
									<textarea class="form-control" name="message" rows="8"></textarea>
									@if ($errors->has('message'))
                      <span class="help-block">
                          <strong>{{ $errors->first('message') }}</strong>
                      </span>
                  @endif
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12 col-sm-9 col-sm-offset-3 text-center">
									<button type="submit" class="btn btn-default">
										Kirim
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 content-box right map-container" data-minheight="250">
				<div id="map" data-animate="true" data-effect="zoomIn">
					{!! $map !!}
				</div>
			</div>
		</section>
	</main>

@endsection
