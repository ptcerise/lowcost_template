@extends('layouts.master')

@section('title')
	{{ ucfirst('Beranda') }}
@endsection

@section('sharing-meta')
@php
	$share_url = Request::url();
	$share_title = $masterName." | ".ucfirst('Beranda');
	$share_desc = $masterDesc;
	foreach($carousel as $key=>$val):
		$share_img = asset($storage_url.'img/home/'.$val->media_url);
		break;
	endforeach;
@endphp
{{-- FACEBOOK --}}
<meta property="og:url" content="{{$share_url}}" />
<meta property="og:type" content="place" />
<meta property="og:title" content="{{$share_title}}" />
<meta property="og:description" content="{{ $share_desc }}" />
<meta property="og:image" content="{{$share_img}}" />

{{-- TWITTER --}}
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="{{$share_url}}">
<meta name="twitter:title" content="{{$share_title}}">
<meta name="twitter:description" content="{{ $share_desc }}">
<meta name="twitter:image:src" content="{{$share_img}}">

{{--GOOGLE PLUS --}}
<meta itemprop="name" content="{{ $share_title }}">
<meta itemprop="description" content="{{ $share_desc }}">
<meta itemprop="image" content="{{$share_img}}">

@endsection

@section('content')
	<header class="container-fluid text-center">
		<h1 class="sr-only">{{ $masterName }}</h1>
		<div id="theCarousel" class="carousel slide carousel-fade" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators hidden-xs">
				@foreach($carousel as $key=>$val)
					<li data-target="#theCarousel" data-slide-to="{{ $key }}" class="{{ ($key == 0)?'active':'' }}"></li>
				@endforeach
			</ol>

			<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">
			@foreach($carousel as $key=>$val)
				<div class="item{{ ($key == 0)?' active':'' }}">
					@php
						$url = asset($storage_url.'img/home/'.$val->media_url);
					@endphp
					<h2 class="carousel-text" data-animate="true" data-effect="slideInRight">
						{{ $val->media_title }}
					</h2>
					<img class="img img-responsive" alt="{{ $val->media_content }}" src="{{ $url }}" />
				</div>
			@endforeach

			{{-- <a class="left carousel-control visible-xs" href="#theCarousel" role="button" data-slide="prev">
		    <i class="fa fa-chevron-left" aria-hidden="true"></i>
		    <span class="sr-only">Previous</span>
		  </a>
		  <a class="right carousel-control visible-xs" href="#theCarousel" role="button" data-slide="next">
		    <i class="fa fa-chevron-right" aria-hidden="true"></i>
		    <span class="sr-only">Next</span>
		  </a> --}}
			</div>
			<div class="slideDown hidden-xs">
				<button id="slideDown" class="btn">
					<i class="fa fa-chevron-down" aria-hidden="true"></i>
				</button>
			</div>
		</div>
	</header>

	<!-- MAIN CONTENT -->
	<main id="mainContent" class="container-fluid home">
		<section class="divider text-center row">
			<div class="col-xs-12" data-animate="true" data-effect="zoomIn">
				<p>{{ $divider1->general_content }}</p>
				<a class="btn btn-default" href="{{ route('section1') }}">{{ ucwords($pageName[0]) }}</a>
			</div>
		</section>

		<section class="row home-content">
			<article class="col-xs-12 col-sm-10 col-sm-offset-1 text-center">
				<header data-animate="true" data-effect="fadeInUp">
					<h2>{{ $content->general_title }}</h2>
				</header>
				<div data-animate="true" data-effect="fadeInUp">
					{!! $content->general_content !!}
				</div>
			</article>
		</section>

		@php
			$url = asset($storage_url.'img/home/'.$banner1->media_url);
		@endphp
		<section class="banner-img row">
			<img alt="banner-background" src="{{ $url }}" />
			<article class="banner-content col-xs-12">
				<div class="content-text">
					<div data-animate="true" data-effect="fadeInUp">
						<header>
							<h2>{{ $banner1->media_title }}</h2>
						</header>
						{!! $banner1->media_content !!}
						<br />
						<a class="btn btn-default" href="{{ route('section1') }}">{{ ucwords($pageName[0]) }}</a>
					</div>
				</div>
			</article>
		</section>

		@if(count($cards) > 0)
		<section id="cards" class="row home-content">
			<div class="col-xs-12 col-sm-10 col-sm-offset-1">
				<div class="row">
				@foreach($cards as $key=>$val)
					<article class="col-xs-12 col-sm-4">
						<div class="card" data-animate="true" data-effect="fadeInUp">
							<img class="img img-responsive" alt="{{ $val->media_desc }}" src="{{ asset($storage_url.'img/home/'.$val->media_url) }}" />
							<div class="card-content">
								<header class="text-center">
									<h3>{{ $val->media_title }}</h3>
								</header>
								{!! $val->media_content !!}
							</div>
						</div>
					</article>
				@endforeach
				</div>
			</div>
		</section>
		@endif

		<section class="divider text-center row">
			<div class="col-xs-12" data-animate="true" data-effect="zoomIn">
				<p>{{ $divider2->general_content }}</p>
				<a class="btn btn-default" href="{{ route('about') }}">{{ ucwords($pageName[2]) }}</a>
			</div>
		</section>

		@php
			$url = asset($storage_url.'img/home/'.$banner2->media_url);
		@endphp
		<section class="banner-img row">
			<img alt="banner-background" src="{{ $url }}" />
			<article class="banner-content col-xs-12">
				<div class="content-text right">
					<div data-animate="true" data-effect="fadeInUp">
						<header>
							<h2>{{ $banner2->media_title }}</h2>
						</header>
						{!! $banner2->media_content !!}
						<br />
						<a class="btn btn-default" href="{{ route('about') }}">{{ ucwords($pageName[2]) }}</a>
					</div>
				</div>
			</article>
		</section>
	</main>

@endsection
