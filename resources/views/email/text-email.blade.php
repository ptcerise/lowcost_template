<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Message from {{ $name }} ( {{ $email }} )</title>
  </head>
  <body>
    <p>
      {{ $content }}
    </p>
  </body>
</html>
