@extends('layouts.master')

@php
	$main_title = $pageName[2];
@endphp
@section('title')
	{{ ucfirst($main_title) }}
@endsection

@section('sharing-meta')
@php
	$share_url = Request::url();
	$share_title = $masterName." | ".ucfirst($main_title);
	if(!empty($contents)){
		foreach($contents as $key=>$val):
			$share_desc = strip_tags($val->media_content);
			break;
		endforeach;
	}else{
		$share_desc = $masterName.' '.$masterDesc;
	}
	$share_img = asset($storage_url.'img/about/'.$banner);
@endphp
{{-- FACEBOOK --}}
<meta property="og:url" content="{{$share_url}}" />
<meta property="og:type" content="place" />
<meta property="og:title" content="{{$share_title}}" />
<meta property="og:description" content="{{ $share_desc }}" />
<meta property="og:image" content="{{$share_img}}" />

{{-- TWITTER --}}
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="{{$share_url}}">
<meta name="twitter:title" content="{{$share_title}}">
<meta name="twitter:description" content="{{ $share_desc }}">
<meta name="twitter:image:src" content="{{$share_img}}">

{{--GOOGLE PLUS --}}
<meta itemprop="name" content="{{ $share_title }}">
<meta itemprop="description" content="{{ $share_desc }}">
<meta itemprop="image" content="{{$share_img}}">

@endsection

@section('content')

	<header class="container-fluid text-center">
		@php
			$url = asset($storage_url.'img/about/'.$banner);
		@endphp
		<div class="banner-section">
			<img alt="banner-header" src="{{ $url }}" />
			<div class="banner-section-content">
				<h1 data-animate="true" data-effect="fadeInUp">
					{{ $main_title }}
				</h1>
			</div>
		</div>
	</header>

	<!-- MAIN CONTENT -->
	<main id="mainContent" class="container-fluid">
	@foreach($contents as $key=>$val)
		@php
			$x = $key+1;
		@endphp
		@if($x % 2 != 0)
			<section class="row section-content">
				<div class="col-xs-12 col-sm-6 content-box left content-image about">
					<div>
						@if(!empty($val->media_title))
						<header class="text-center visible-xs" data-animate="true" data-effect="fadeInUp">
							<h2>{{ $val->media_title }}</h2>
						</header>
						@endif
						<div data-animate="true" data-effect="fadeInUp">
							@php
								$url = asset($storage_url.'img/about/'.$val->media_url);
							@endphp
							<img class="img img-responsive" alt="image" src="{{ $url }}">
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 content-box right content-text about">
					<div>
						@if(!empty($val->media_title))
						<header class="text-center hidden-xs" data-animate="true" data-effect="fadeInUp">
							<h2>{{ $val->media_title }}</h2>
						</header>
						@endif
						<div data-animate="true" data-effect="fadeInUp">
							{!! $val->media_content !!}
						</div>
					</div>
				</div>
			</section>
		@else
			<section class="row section-content">
				<div class="col-xs-12 col-sm-6 content-box right content-image pull-right about">
					<div>
						@if(!empty($val->media_title))
						<header class="text-center visible-xs" data-animate="true" data-effect="fadeInUp">
							<h2>{{ $val->media_title }}</h2>
						</header>
						@endif
						<div data-animate="true" data-effect="fadeInUp">
							@php
								$url = asset($storage_url.'img/about/'.$val->media_url);
							@endphp
							<img class="img img-responsive" alt="image" src="{{ $url }}">
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 content-box left content-text about">
					<div>
						@if(!empty($val->media_title))
						<header class="text-center hidden-xs" data-animate="true" data-effect="fadeInUp">
							<h2>{{ $val->media_title }}</h2>
						</header>
						@endif
						<div data-animate="true" data-effect="fadeInUp">
							{!! $val->media_content !!}
						</div>
					</div>
				</div>
			</section>
		@endif
	@endforeach

	</main>

@endsection
