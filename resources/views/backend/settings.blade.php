@extends('layouts.masterBackend')

@section('title')
    {{ ucfirst('settings') }}
@endsection

@section('content')
    <!-- MAIN CONTENT -->
    <div class="container-fluid">
	    <div class="row">
        <div class="col-xs-12">
          {!! Form::open(['url'=>route('saveProfile'),'method'=>'post','files'=>true]) !!}
          <section id="profile" class="panel panel-primary">
          <header class="panel-heading">
            <h2>Profile</h2>
          </header>
          <div class="panel-body">
            @if(Session::has('message'))
              <div class="alert {{ Session::get('alert-class') }} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <p>{{ Session::get('message') }}</p>
              </div>
            @endif
            <div class="form-group col-xs-12 col-sm-6">
              <div class="col-xs-12 image-with-preview col-sm-6">
                <img class="prev-img img-bordered img-responsive" alt="logo" src="{{ !empty($masterLogo)?asset($storage_url.'img/'.$masterLogo):'http://via.placeholder.com/200x200' }}">
                {!! Form::label('companyLogo','Choose an Image for Logo',['class'=>'control-label']) !!}
                <div class="{{ $errors->has('companyLogo') ? ' has-error' : '' }}">
                  {!! Form::hidden('companyLogoId',$companyLogo->id) !!}
                  {!! Form::file('companyLogo',['class'=>'form-control','data-fileinput'=>'image']) !!}
                  @if ($errors->has('companyLogo'))
                      <span class="help-block">
                          <strong>{{ $errors->first('companyLogo') }}</strong>
                      </span>
                  @endif
                  <br>
                  <div class="upload-rules text-left">
                    <ul>
                      <li>Min resolution <strong>200 x 200 px</strong></li>
                      <li>File type <strong>*.png</strong></li>
                      <li>Max file size <strong>1mb</strong></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 image-with-preview col-sm-6">
                <img class="prev-img img-bordered img-responsive" alt="Favicon" src="{{ !empty($masterFavicon->setting_value)?asset($storage_url.'img/'.$masterFavicon->setting_value):'http://via.placeholder.com/200x200' }}" style="padding:84px">
                {!! Form::label('companyFavicon','Choose an Image for Favicon',['class'=>'control-label']) !!}
                <div class="{{ $errors->has('companyFavicon') ? ' has-error' : '' }}">
                  {!! Form::hidden('companyFaviconId',$companyFavicon->id) !!}
                  @php
                    $fv = explode('=',$companyFavicon->setting_desc);
                    $version = (int)$fv[1];
                  @endphp
                  {!! FOrm::hidden('faviconVersion', $version) !!}
                  {!! Form::file('companyFavicon',['class'=>'form-control','data-fileinput'=>'image']) !!}
                  @if ($errors->has('companyFavicon'))
                      <span class="help-block">
                          <strong>{{ $errors->first('companyFavicon') }}</strong>
                      </span>
                  @endif
                  <br>
                  <div class="upload-rules text-left">
                    <ul>
                      <li>Min resolution <strong>32 x 32 px</strong></li>
                      <li>File type <strong>*.ico</strong></li>
                      <li>Max file size <strong>50kb</strong></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group col-xs-12 col-sm-6{{ $errors->has('companyName') ? ' has-error' : '' }}">
              <label class="control-label">
                <i class="fa fa-id-badge" aria-hidden="true"></i> Company Name
              </label>
              {!! Form::hidden('companyNameId',$companyName->id) !!}
              {!! Form::text('companyName',$companyName->setting_value,['class'=>'form-control']) !!}
              @if ($errors->has('companyName'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('companyName') }}</strong>
                                </span>
                            @endif
            </div>
            <div class="form-group col-xs-12 col-sm-6{{ $errors->has('companyDesc') ? ' has-error' : '' }}">
              <label class="control-label">
                <i class="fa fa-info-circle" aria-hidden="true"></i> Company Description
              </label>
              {!! Form::hidden('companyDescId',$companyDesc->id) !!}
              {!! Form::textarea('companyDesc',$companyDesc->setting_value,['class'=>'form-control not-tinymce','max'=>'255']) !!}

              @if ($errors->has('companyDesc'))
                <span class="help-block">
                    <strong>{{ $errors->first('companyDesc') }}</strong>
                </span>
            @endif
            </div>
          </div>
          <footer class="panel-footer text-right">
            <button type="submit" class="btn btn-primary">Update</button>
          </footer>
          </section>
          {!! Form::close() !!}
        </div>

        <div class="col-xs-12">
          {!! Form::open(['url'=>route('saveFont'),'method'=>'post']) !!}
          <section id="profile" class="panel panel-primary">
          <header class="panel-heading">
            <h2>Font Setting</h2>
          </header>
          <div class="panel-body">
            @if(Session::has('message-fontselect'))
              <div id="message-fontselect" class="alert {{ Session::get('alert-class') }} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <p>{{ Session::get('message-fontselect') }}</p>
              </div>
            @endif
            <div class="col-xs-12 col-sm-6 font-selector">

              <div class="panel panel-primary">
                <div class="panel-heading">
                  <h3>Global Font</h3>
                </div>
                <div class="panel-body">
                  <div class="row">
                    <div class="form-group col-xs-12 col-sm-6">
                      {!! Form::label('fontselect[]','Select a Font',['class'=>'control-label']) !!}
                      <div class="">
                        {!! Form::text('fontselect[]',$webFont[0],['class'=>'form-control fontselect']) !!}
                      </div>
                    </div>
                    <div class="form-group col-xs-12 col-sm-6">
                      <div class="well text-center">
                        <p class="fontExample">Example text</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

            </div>
            <div class="col-xs-12 col-sm-6 font-selector">
              <div class="panel panel-primary">
                <div class="panel-heading">
                  <h3>Navbar Font</h3>
                </div>
                <div class="panel-body">
                  <div class="row">
                    <div class="form-group col-xs-12 col-sm-6">
                      {!! Form::label('fontselect[]','Select a Font',['class'=>'control-label']) !!}
                      <div class="">
                        {!! Form::text('fontselect[]',$webFont[1],['class'=>'form-control fontselect']) !!}
                      </div>
                    </div>
                    <div class="form-group col-xs-12 col-sm-6">
                      <div class="well text-center">
                        <p class="fontExample">Example text</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 font-selector">

              <div class="panel panel-primary">
                <div class="panel-heading">
                  <h3>Content Font</h3>
                </div>
                <div class="panel-body">
                  <div class="row">
                    <div class="form-group col-xs-12 col-sm-6">
                      {!! Form::label('fontselect[]','Select a Font',['class'=>'control-label']) !!}
                      <div class="">
                        {!! Form::text('fontselect[]',$webFont[2],['class'=>'form-control fontselect']) !!}
                      </div>
                    </div>
                    <div class="form-group col-xs-12 col-sm-6">
                      <div class="well text-center">
                        <p class="fontExample">Example text</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

            </div>
            <div class="col-xs-12 col-sm-6 font-selector">
              <div class="panel panel-primary">
                <div class="panel-heading">
                  <h3>Heading (h1, h2, h3, h4) Font</h3>
                </div>
                <div class="panel-body">
                  <div class="row">
                    <div class="form-group col-xs-12 col-sm-6">
                      {!! Form::label('fontselect[]','Select a Font',['class'=>'control-label']) !!}
                      <div class="">
                        {!! Form::text('fontselect[]',$webFont[3],['class'=>'form-control fontselect']) !!}
                      </div>
                    </div>
                    <div class="form-group col-xs-12 col-sm-6">
                      <div class="well text-center">
                        <p class="fontExample">Example text</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 font-selector">
              <div class="panel panel-primary">
                <div class="panel-heading">
                  <h3>Image Caption & Sub-Caption Font</h3>
                </div>
                <div class="panel-body">
                  <div class="row">
                    <div class="col-xs-12 col-sm-4">
                      <div class="form-group col-xs-12">
                        {!! Form::label('fontselect[]','Select a Caption Font',['class'=>'control-label']) !!}
                        <div class="">
                          {!! Form::text('fontselect[]',$webFont[4],['class'=>'form-control fontselect','data-id'=>'caption']) !!}
                        </div>
                      </div>
                      <div class="form-group col-xs-12">
                        {!! Form::label('fontselect[]','Select a Sub-Caption Font',['class'=>'control-label']) !!}
                        <div class="">
                          {!! Form::text('fontselect[]',$webFont[5],['class'=>'form-control fontselect','data-id'=>'subcaption']) !!}
                        </div>
                      </div>
                    </div>
                    <div class="form-group col-xs-12 col-sm-8">
                      <div class="">
                        <div class="banner-img" style="height:140px;">
                          <img src="https://placeimg.com/750/140/arch">
                          <div class="banner-content">
                            <div class="content-text" style="text-align: left;padding:50px">
                              <h4 id="captionFont" style="font-size: 32px;">Lorem ipsum dolor sit</h4>
                              <p id="subCaptionFont" style="font-size: 16px;">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 font-selector">

              <div class="panel panel-primary">
                <div class="panel-heading">
                  <h3>Company Name Font</h3>
                </div>
                <div class="panel-body">
                  <div class="row">
                    <div class="form-group col-xs-12 col-sm-6">
                      {!! Form::label('fontselect[]','Select a Font',['class'=>'control-label']) !!}
                      <div class="">
                        {!! Form::text('fontselect[]',$webFont[6],['class'=>'form-control fontselect']) !!}
                      </div>
                    </div>
                    <div class="form-group col-xs-12 col-sm-6">
                      <div class="well text-center">
                        <p class="fontExample">Example text</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

            </div>
            <div class="col-xs-12 col-sm-6 font-selector">
              <div class="panel panel-primary">
                <div class="panel-heading">
                  <h3>Button Font</h3>
                </div>
                <div class="panel-body">
                  <div class="row">
                    <div class="form-group col-xs-12 col-sm-6">
                      {!! Form::label('fontselect[]','Select a Font',['class'=>'control-label']) !!}
                      <div class="">
                        {!! Form::text('fontselect[]',$webFont[7],['class'=>'form-control fontselect']) !!}
                      </div>
                    </div>
                    <div class="form-group col-xs-12 col-sm-6">
                      <div class="well text-center">
                        <a class="fontExample btn btn-primary">Example text</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <footer class="panel-footer text-right">
            <button type="submit" class="btn btn-primary">Update</button>
            <button id="defaultFont" class="btn btn-default">Reset Font</button>
          </footer>
          </section>
          {!! Form::close() !!}
        </div>

        <div class="col-xs-12">
          <div class="panel panel-primary">
            <div class="panel-heading">
              <h2>Color Setting</h2>
            </div>
            {!! Form::open(['url' => url('backend/settings/save_color'),'method' => 'post']) !!}
            {!! Form::hidden('idcolor',App\Settings::where(['setting_name'=>'webColor'])->first()->id) !!}
            <div class="panel-body">
              @if(Session::has('message-color'))
                <div id="message-color" class="alert {{ Session::get('alert-class') }} alert-dismissible" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <p>{{ Session::get('message-color') }}</p>
                </div>
              @endif
              <div class="form-group col-xs-12 col-sm-3">
                {!! Form::label('color[]','Main Color ',['class'=>'control-label']) !!}
                <div class="">
                  {!! Form::text('color[]',$webColor[0],['class'=>'form-control jscolor']) !!}
                </div>
              </div>
              <div class="form-group col-xs-12 col-sm-3">
                {!! Form::label('color[]','Background Color ',['class'=>'control-label']) !!}
                <div class="">
                  {!! Form::text('color[]',$webColor[1],['class'=>'form-control jscolor']) !!}
                </div>
              </div>
              <div class="form-group col-xs-12 col-sm-3">
                {!! Form::label('color[]','Foreground Color ',['class'=>'control-label']) !!}
                <div class="">
                  {!! Form::text('color[]',$webColor[2],['class'=>'form-control jscolor']) !!}
                </div>
              </div>
              <div class="form-group col-xs-12 col-sm-3">
                {!! Form::label('color[]','Text Color ',['class'=>'control-label']) !!}
                <div class="">
                  {!! Form::text('color[]',$webColor[3],['class'=>'form-control jscolor']) !!}
                </div>
              </div>

              <div class="col-xs-12">
                <hr/>
              </div>

              {{-- BUTTON COLOR --}}
              {!! Form::hidden('idbtncolor',App\Settings::where(['setting_name'=>'btnColor'])->first()->id) !!}
              <div class="form-group col-xs-12 col-sm-3">
                {!! Form::label('btn_color[]','Button Background Color ',['class'=>'control-label']) !!}
                <div class="">
                  {!! Form::text('btn_color[]',$btnColor[0],['class'=>'form-control jscolor','data-id'=>'bgcolor']) !!}
                </div>
              </div>
              <div class="form-group col-xs-12 col-sm-3">
                {!! Form::label('btn_color[]','Button Text Color ',['class'=>'control-label']) !!}
                <div class="">
                  {!! Form::text('btn_color[]',$btnColor[1],['class'=>'form-control jscolor','data-id'=>'txtcolor']) !!}
                </div>
              </div>
              <div class="form-group col-xs-12 col-sm-3">
                {!! Form::label('btn_color[]','Button Background Color (Hover) ',['class'=>'control-label']) !!}
                <div class="">
                  {!! Form::text('btn_color[]',$btnColor[2],['class'=>'form-control jscolor','data-id'=>'bgcolorhover']) !!}
                </div>
              </div>

              <div class="form-group col-xs-12 col-sm-3">
                {!! Form::label('btn_color[]','Button Text Color (Hover) ',['class'=>'control-label']) !!}
                <div class="">
                  {!! Form::text('btn_color[]',$btnColor[3],['class'=>'form-control jscolor','data-id'=>'txtcolorhover']) !!}
                </div>
              </div>
              <style id="styleBtnBgColor">
                #btnExample{background-color: #{{ $btnColor[0] }}}
              </style>
              <style id="styleBtnTxtColor">
                #btnExample{color: #{{ $btnColor[1] }}}
              </style>
              <style id="styleBtnBgColorHover">
                #btnExample:hover{background-color: #{{ $btnColor[2] }}}
              </style>
              <style id="styleBtnTxtColorHover">
                #btnExample:hover{color: #{{ $btnColor[3] }}}
              </style>
              <div class="form-group col-xs-12 text-center">
                <label class="control-label">Example</label>
                <div class="well">
                  <a id="btnExample" class="btn btn-default btn-lg">Example Button</a>
                </div>
              </div>

              <div class="col-xs-12">
                <hr/>
              </div>

              {{-- TEXT COLOR --}}
              {!! Form::hidden('idtxtbannercolor',App\Settings::where(['setting_name'=>'txtBannerColor'])->first()->id) !!}
              <div class="col-xs-12 col-sm-6">
                <div class="well">
                  <div class="row">
                    <div class="form-group col-xs-12 col-sm-6">
                      {!! Form::label('text_onbanner','Text On Banner Color ',['class'=>'control-label']) !!}
                      <div class="">
                        {!! Form::text('text_onbanner',$txtBannerColor,['class'=>'form-control jscolor']) !!}
                      </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                      <label class="coltrol-label">Example</label>
                      <div id="textOnBannerExample" class="banner-img">
                        <img src="https://placeimg.com/250/75/arch">
                        <div class="banner-content">
                          <div class="content-text" style="color : #{{$txtBannerColor}}">
                            <h2>Example Text</h2>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              {{-- TEXT COLOR --}}
              {!! Form::hidden('idnavbarcolor',App\Settings::where(['setting_name'=>'navbarColor'])->first()->id) !!}
              <div class="col-xs-12 col-sm-6">
                <div class="well">
                  <div class="row">
                    <div class="form-group col-xs-12 col-sm-6" style="margin-bottom: 2px">
                      {!! Form::label('panel_color[]','Navbar / Divider / Card Color ',['class'=>'control-label']) !!}
                      <i class="fa fa-question-circle" aria-hidden="true" data-toggle="popover" data-placement="top" title="Navbar / Divider / Card Color" data-content="Select 2 different colors to make it gradient, select 2 same colors to make it 1 color"></i>
                      <div class="">
                        {!! Form::text('panel_color[]',$navbarColor[0],['class'=>'form-control jscolor','data-id'=>'color1']) !!}
                      </div>
                      <div class="" style="margin-top:5px">
                        {!! Form::text('panel_color[]',$navbarColor[1],['class'=>'form-control jscolor','data-id'=>'color2']) !!}
                      </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                      <style id="panelColorStyle">
                        #panelColor{
                          background: #{{$navbarColor[0]}};
                          background: -webkit-linear-gradient(#{{$navbarColor[0]}}, #{{$navbarColor[1]}});
                          background: -o-linear-gradient(#{{$navbarColor[0]}}, #{{$navbarColor[1]}});
                          background: -moz-linear-gradient(#{{$navbarColor[0]}}, #{{$navbarColor[1]}});
                          background: linear-gradient(#{{$navbarColor[0]}}, #{{$navbarColor[1]}});
                        }
                      </style>
                      <label class="coltrol-label">Example</label>
                      <div id="panelColor">
                      </div>
                    </div>
                  </div>
                </div>
              </div>

            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-primary">Update</button>
            </div>
            {!! Form::close() !!}
          </div>
        </div>

        <div class="col-xs-12">
          <div class="panel panel-primary">
            <div class="panel-heading">
              <h2>Page Name</h2>
            </div>
            {!! Form::open(['url' => url('backend/settings/save_page_name'),'method' => 'post']) !!}
            {!! Form::hidden('id',App\Settings::where(['setting_name'=>'pageName'])->first()->id) !!}
            <div class="panel-body">
              @if(Session::has('message-pagename'))
                <div id="message-pagename" class="alert {{ Session::get('alert-class') }} alert-dismissible" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <p>{{ Session::get('message-pagename') }}</p>
                </div>
              @endif
              @for($i=0;$i<4;$i++)
              <div class="form-group">
                {!! Form::label('pagename','Page '.($i+1).' : '.ucwords($pageName[$i]),['class'=>'control-label']) !!}
                <div class="">
                  {!! Form::text('pagename[]',ucwords($pageName[$i]),['class'=>'form-control']) !!}
                </div>
              </div>
              @endfor
            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-primary">Update</button>
            </div>
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
@endsection

@section('script')
  <script type="text/javascript">
    $("#text_onbanner").on("change",function() {
      var $color = $(this).val();
      $("#textOnBannerExample").find(".content-text").css("color","#"+$color);
    });

    $("input[name='btn_color[]']").each(function() {
      $($(this)).on('change',function(){
        var $id = $(this).data('id');
        var $color = $(this).val();
        if($id == "bgcolor"){
          $("#styleBtnBgColor").html('#btnExample{background-color: #'+$color+'}');
        }else if ($id == "bgcolorhover") {
          $("#styleBtnBgColorHover").html('#btnExample:hover{background-color: #'+$color+'}');
        }else if ($id == "txtcolor") {
          $("#styleBtnTxtColor").html('#btnExample{color: #'+$color+'}');
        }else if ($id == "txtcolorhover") {
          $("#styleBtnTxtColorHover").html('#btnExample:hover{color: #'+$color+'}');
        }
      });
    });

    $("input[name='panel_color[]']").each(function() {
      $($(this)).on('change',function(){
        var $color1 = $("input[data-id='color1']").val();
        var $color2 = $("input[data-id='color2']").val();
        $("#panelColorStyle").html("#panelColor{background: #"+$color1+";background: -webkit-linear-gradient(#"+$color1+", #"+$color2+");background: -o-linear-gradient(#"+$color1+", #"+$color2+");background: -moz-linear-gradient(#"+$color1+", #"+$color2+");background: linear-gradient(#"+$color1+", #"+$color2+");}");
      });
    });

    $(document).ready(function(){
        $('[data-toggle="popover"]').popover();

        $('.fontselect').each(function(){
            var $id = $(this).data('id');

            // replace + signs with spaces for css
            var font = $($(this)).val().replace(/\+/g, ' ');

            // split font into family and weight
            font = font.split(':');

            // set family on paragraphs
            if($id == "caption"){
              $('#captionFont').css('font-family', font[0]);
            }else if($id == "subcaption"){
              $('#subCaptionFont').css('font-family', font[0]);
            }else{
              $($(this)).parents('.font-selector').find('.fontExample').css('font-family', font[0]);
            }
        });
    });

    $('.fontselect').fontselect().each(function(){
      $($(this)).change(function(){

        var $id = $(this).data('id');

        // replace + signs with spaces for css
        var font = $(this).val().replace(/\+/g, ' ');

        // split font into family and weight
        font = font.split(':');

        // set family on paragraphs
        if($id == "caption"){
          $('#captionFont').css('font-family', font[0]);
        }else if($id == "subcaption"){
          $('#subCaptionFont').css('font-family', font[0]);
        }else{
          $(this).parents('.font-selector').find('.fontExample').css('font-family', font[0]);
        }
      });
    });

    $("#defaultFont").click(function(){
      $('.fontselect').each(function(){
        $($(this)).val("");
      });
    });
  </script>
@endsection
