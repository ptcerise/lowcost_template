@extends('layouts.masterBackend')

@section('title')
	@if($page == "section1")
    	{{ ucwords($pageName[0]) }}
    @else
    	{{ ucwords($pageName[1]) }}
    @endif
@endsection

@section('content')
    <!-- MAIN CONTENT -->
    <div class="container-fluid">
		{{-- BANNER --}}
		<div class="row">
    		<div class="col-xs-12">
				<section id="message-banner" class="panel panel-primary">
					<header class="panel-heading">
						<h2>
							Banner Header
						</h2>
					</header>
					{!! Form::open(['url'=>'backend/sections/save_banner','method'=>'post','files'=>true]) !!}
					{!! Form::hidden('section_name',$page) !!}
					{!! Form::hidden('id',$banner->id) !!}
					<div class="panel-body">
						@if(Session::has('message-banner'))
							<div class="alert {{ Session::get('alert-class') }} alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							 	<p>{{ Session::get('message-banner') }}</p>
							</div>
						@endif

						<div class="form-group image-with-preview col-xs-12 col-sm-6 col-sm-offset-3">
							<img class="prev-img img-bordered img-responsive" alt="banner" src="{{ isset($banner->media_url)?asset($storage_url.'img/sections/'.$banner->media_url):asset($storage_url.'img/img4x3.jpg') }}">
							{!! Form::label('banner_section','Choose an Image',['class'=>'control-label']) !!}
							<div class="{{ $errors->has('banner_section') ? ' has-error' : '' }}">
								{!! Form::file('banner_section',['class'=>'form-control','data-fileinput'=>'image']) !!}
								@if ($errors->has('banner_section'))
                    <span class="help-block">
                        <strong>{{ $errors->first('banner_section') }}</strong>
                    </span>
                @endif
                <br>
								<div class="upload-rules text-left">
									<ul>
										<li>Min resolution <strong>1600 x 320 px</strong></li>
										<li>File type <strong>*.jpg / *.jpeg</strong></li>
										<li>Max file size <strong>1mb</strong></li>
									</ul>
								</div>
							</div>

							<div class="form-group">
								{!! Form::label('banner_caption','Caption',['class'=>'control-label']) !!}
								<div class="{{ $errors->has('banner_caption') ? ' has-error' : '' }}">
									{!! Form::text('banner_caption', $banner->media_title,['class'=>'form-control']) !!}
									@if ($errors->has('banner_section'))
	                    <span class="help-block">
	                        <strong>{{ $errors->first('banner_caption') }}</strong>
	                    </span>
	                @endif
								</div>
							</div>

						</div>

					</div>
					<footer class="panel-footer text-center">
						<button type="submit" class="btn btn-primary">Update</button>
					</footer>
					{!! Form::close() !!}
				</section>
    		</div>
    	</div>
	    {{-- CONTENT --}}
    	<div class="row">
    		<div class="col-xs-12">
				<section id="message-content" class="panel panel-primary">
					<header class="panel-heading">
						<h2>
							Contents
							<button class="btn btn-success" data-toggle="modal" data-target="#addContent">Add Content</button>
							<button class="btn btn-warning" data-toggle="modal" data-target="#reorder">Arrange order</button>
						</h2>
					</header>
					<div class="panel-body">
						@if(Session::has('message-content'))
							<div class="alert {{ Session::get('alert-class') }} alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							 	<p>{{ Session::get('message-content') }}</p>
							</div>
						@endif
						@if($errors->any())
							<div id="message-content" class="alert alert-danger alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<ul>
								 	@foreach ($errors->all() as $error)
								        <li>{{ $error }}</li>
								    @endforeach
							    </ul>
							</div>
						@endif
						<div class="table-responsive">
				    		<table id="contentsTable" class="table table-hover">
							    <thead>
								    <tr>
								    	<th class="text-center">#</th>
								        <th class="text-center">Image</th>
								        <th class="text-center">Title</th>
								        <th class="text-center" width="30%">Content</th>
								        <th class="text-center">Options</th>
								    </tr>
							    </thead>
							    <tbody>
								    @foreach($contents as $key => $val)
									    <tr>
									    	<td class="text-center">{{$key+1}}</td>
									        <td class="text-center">
									        	<img class="img img-responsive" alt="image" src="{{ asset($storage_url.'img/sections/'.$val->media_url) }}" style="max-height: 120px;margin: 0 auto">
									        </td>
									        <td>
									        	{{ $val->media_title }}
									        </td>
									        <td>
									        	{!! $val->media_content !!}
									        </td>
									        <td class="text-center">
												{{-- Update data --}}
												<button title="Update content" class="btn btn-warning" data-toggle="modal" data-target="#update{{ $val->id }}"><i class="fa fa-pencil" aria-hidden="true"></i></button>

									        	{{-- Delete data --}}
												<button title="Delete content" class="btn btn-danger" data-toggle="modal" data-target="#delete{{ $val->id }}"><i class="fa fa-trash" aria-hidden="true"></i></button>
												<div id="delete{{ $val->id }}" class="modal fade" role="dialog">
													<div class="modal-dialog modal-sm">

													{!! Form::open(['url' => url('backend/sections/delete_content/'.$val->id),'method'=>'delete']) !!}
														{!! Form::hidden('id',$val->id) !!}
														<div class="modal-content text-center">
															<div class="modal-body">
																<h3>Delete content ?</h3>
																<button type="submit" class="btn btn-primary">Delete</button>
																<button class="btn btn-default" data-dismiss="modal">Cancel</button>
															</div>
														</div>
													{!! Form::close() !!}

													</div>
												</div>
									        </td>
									    </tr>
									@endforeach
							    </tbody>
							</table>
				    	</div>
					</div>

					@foreach ($contents as $key => $val)
						<div id="update{{ $val->id }}" class="modal fade" role="dialog">
							<div class="modal-dialog modal-lg">

							{!! Form::open(['url' => url('backend/sections/update_content/'.$val->id),'method'=>'post','files'=>true]) !!}
							{!! Form::hidden('id',$val->id) !!}
							{!! Form::hidden('section',$page) !!}
							<div class="modal-content text-center">
								<div class="modal-body container-fluid">
									<div class="col-xs-12 col-sm-6">
										<div class="form-group image-with-preview">
											<img class="prev-img img-bordered img-responsive" alt="banner" src="{{ isset($val->media_url)?asset($storage_url.'img/sections/'.$val->media_url):asset($storage_url.'img/img4x3.jpg') }}">
											{!! Form::label('image_content','Choose an Image',['class'=>'control-label']) !!}
											<div class="">
												{!! Form::file('image_content',['class'=>'form-control','data-fileinput'=>'image']) !!}
																				<br>
												<div class="upload-rules text-left">
													<ul>
														<li>Min resolution <strong>800 x 600 px</strong></li>
														<li>File type <strong>*.jpg / *.jpeg</strong></li>
														<li>Max file size <strong>1mb</strong></li>
													</ul>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-6">
										<div class="form-group">
											{!! Form::label('title_content','Title',['class'=>'control-label']) !!}
											<div class="">
												{!! Form::text('title_content',$val->media_title,['class'=>'form-control']) !!}
											</div>
										</div>
										<div class="form-group">
											{!! Form::label('text_content','Text Content',['class'=>'control-label']) !!}
											<div class="">
												{!! Form::textarea('text_content',$val->media_content,['class'=>'form-control']) !!}
											</div>
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<button type="submit" class="btn btn-primary">Save</button>
									<button class="btn btn-default" data-dismiss="modal">Cancel</button>
								</div>
							</div>
							{!! Form::close() !!}

							</div>
						</div>
					@endforeach

					<div id="addContent" class="modal fade" role="dialog">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="modal-header">
									<h2>Add Content</h2>
								</div>
								{!! Form::open(['url' => url('backend/sections/save_content'),'method' => 'post','files'=>true]) !!}
								{!! Form::hidden('section',$page) !!}
								<div class="modal-body container-fluid">
									@if(Session::has('message-addcontent'))
										<div id="message-addcontent" class="alert {{ Session::get('alert-class') }} alert-dismissible" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										 	<p>{{ Session::get('message-addcontent') }}</p>
										</div>
									@endif

									<div class="col-xs-12 col-sm-6">
										<div class="form-group image-with-preview">
											<img class="prev-img img-bordered img-responsive" alt="banner" src="{{ asset($storage_url.'img/img4x3.jpg') }}">
											{!! Form::label('image_content','Choose an Image',['class'=>'control-label']) !!}
											<div class="">
												{!! Form::file('image_content',['class'=>'form-control','data-fileinput'=>'image']) !!}
				                                <br>
												<div class="upload-rules text-left">
													<ul>
														<li>Min resolution <strong>800 x 600 px</strong></li>
														<li>File type <strong>*.jpg / *.jpeg</strong></li>
														<li>Max file size <strong>1mb</strong></li>
													</ul>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-6">
										<div class="form-group">
											{!! Form::label('title_content','Title',['class'=>'control-label']) !!}
											<div class="">
												{!! Form::text('title_content','',['class'=>'form-control']) !!}
											</div>
										</div>
										<div class="form-group">
											{!! Form::label('text_content','Text Content',['class'=>'control-label']) !!}
											<div class="">
												{!! Form::textarea('text_content','',['class'=>'form-control']) !!}
											</div>
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<button type="submit" class="btn btn-primary">Save</button>
									<button class="btn btn-default" data-dismiss="modal">Cancel</button>
								</div>
								{!! Form::close() !!}
							</div>
						</div>
					</div>
				</section>

				{{-- reorder --}}
        <div id="reorder" class="modal fade" role="dialog">
        <div class="modal-dialog">

        {!! Form::open(['url' => url('backend/sections/order_content'),'method'=>'post']) !!}
          <div class="modal-content">
            <div class="modal-header">
              <h2>Rearrange order</h2>
            </div>
            <div class="modal-body text-center">
              @foreach ($contents as $x => $vals)
              <div class="form-group text-left">
                <div class="row">

                <div class="col-xs-12 col-sm-1">
                  {!! Form::label('reorder',$x+1,['class'=>'control-label']) !!}
                </div>
                <div class="col-xs-12 col-sm-11">
                  <select name="reorder[]" class="image-select form-control">
                      @foreach ($contents as $key => $val)
                      <option
                          value="{{ $val->id }}"
                          data-img-src="{{ asset($storage_url.'img/sections/'.$val->media_url) }}"
                          @if ($x == $key)
                            selected="selected"
                          @endif
                      >
                          ( {{ $key+1 }} ) {{ $val->media_title }}
                      </option>
                    @endforeach
                  </select>
                </div>
                </div>
              @endforeach
              </div>
            </div>
            <footer class="modal-footer">
              <button type="submit" class="btn btn-primary">Save</button>
              <button class="btn btn-default" data-dismiss="modal">Cancel</button>
            </footer>
          </div>
        {!! Form::close() !!}

        </div>
        </div>
			</div>
    	</div>
    </div>
@endsection

@section('script')
<script type="text/javascript">
	$("#contentsTable").DataTable();
	$(".image-select").chosen();

	$(document).ready(function(){
    $("#publishBtn").click(function(){
				var $page = $(this).attr('data-page');
				var $stat = $(this).attr('data-publish');
				if($stat == "publish"){
					var $newstat = "hidden";
				}else{
					var $newstat = "publish";
				}

        $.post("{{url('/backend/settings/page_publish')}}",
        {
					_token: "{{csrf_token()}}",
          page: $page,
          stat: $newstat,
        },
        function(data,status){
            if(status == "success"){
							var obj = $.parseJSON(data);
							if(obj.stat == "publish"){
								$("#publishBtn").find("i").removeClass('fa-eye-slash').addClass('fa-eye');
								$("#publishBtn").attr("data-publish","publish");
								$("#publishBtn").attr("title","This page is published");
								$("#"+obj.page+"stat").find("i").removeClass('fa-eye-slash').addClass('fa-eye');
								$("#"+obj.page+"stat").attr("title","This page is published");
							}else{
								$("#publishBtn").find("i").removeClass('fa-eye').addClass('fa-eye-slash');
								$("#publishBtn").attr("data-publish","hidden");
								$("#publishBtn").attr("title","This page is not published");
								$("#"+obj.page+"stat").find("i").removeClass('fa-eye').addClass('fa-eye-slash');
								$("#"+obj.page+"stat").attr("title","This page is not published");
							}
						}
        });
    });
});
</script>
@endsection
