@extends('layouts.masterBackend')

@section('title')
    {{ ucfirst('contact') }}
@endsection

@section('content')
    <!-- MAIN CONTENT -->
    <div class="container-fluid">
    {{-- BANNER --}}
		<div class="row">
    		<div class="col-xs-12">
				<section id="message-banner" class="panel panel-primary">
					<header class="panel-heading">
						<h2>
							Banner Header
						</h2>
					</header>
					{!! Form::open(['url'=>'backend/contact/save_banner','method'=>'post','files'=>true]) !!}
					{!! Form::hidden('id',$banner->id) !!}
					<div class="panel-body">
						@if(Session::has('message-banner'))
							<div class="alert {{ Session::get('alert-class') }} alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							 	<p>{{ Session::get('message-banner') }}</p>
							</div>
						@endif

						<div class="form-group image-with-preview col-xs-12 col-sm-6 col-sm-offset-3">
							<img class="prev-img img-bordered img-responsive" alt="banner" src="{{ isset($banner->media_url)?asset($storage_url.'img/contact/'.$banner->media_url):asset($storage_url.'img/img4x3.jpg') }}">
							{!! Form::label('banner_section','Choose an Image',['class'=>'control-label']) !!}
							<div class="{{ $errors->has('banner_section') ? ' has-error' : '' }}">
								{!! Form::file('banner_section',['class'=>'form-control','data-fileinput'=>'image']) !!}
								@if ($errors->has('banner_section'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('banner_section') }}</strong>
                                    </span>
                                @endif
                                <br>
								<div class="upload-rules text-left">
									<ul>
										<li>Min resolution <strong>1600 x 320 px</strong></li>
										<li>File type <strong>*.jpg / *.jpeg</strong></li>
										<li>Max file size <strong>1mb</strong></li>
									</ul>
								</div>
							</div>
						</div>

					</div>
					<footer class="panel-footer text-center">
						<button type="submit" class="btn btn-primary">Update</button>
					</footer>
					{!! Form::close() !!}
				</section>
    		</div>
    	</div>
	    <div class="row">
	    	<div class="col-xs-12">
	    	<form method="POST" action="{{route('saveContactInfo')}}">
	    	{{ csrf_field() }}
		    	<section id="contact-info" class="panel panel-primary">
					<header class="panel-heading">
						<h2>Contact Info</h2>
					</header>
					<div class="panel-body">
						@if(Session::has('message-contact-info'))
							<div class="alert {{ Session::get('alert-class') }} alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							 	<p>{{ Session::get('message-contact-info') }}</p>
							</div>
						@endif
						<div class="form-group col-xs-12 col-sm-6{{ $errors->has('companyEmail') ? ' has-error' : '' }}">
							<label class="control-label">
								<i class="fa fa-envelope" aria-hidden="true"></i> Company E-mail
							</label>
							<input type="hidden" name="companyEmailId" value="{{$companyEmail->id}}">
							<input class="form-control" type="email" name="companyEmail" value="{{ $companyEmail->setting_value }}">

							@if ($errors->has('companyEmail'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('companyEmail') }}</strong>
                                </span>
                            @endif
						</div>
						<div class="form-group col-xs-12 col-sm-6{{ $errors->has('companyPhone') ? ' has-error' : '' }}">
							<label class="control-label">
								<i class="fa fa-phone" aria-hidden="true"></i> Company Phone
							</label>
							<input type="hidden" name="companyPhoneId" value="{{$companyPhone->id}}">
							<input class="form-control" type="text" name="companyPhone" value="{{ $companyPhone->setting_value }}">

							@if ($errors->has('companyPhone'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('companyPhone') }}</strong>
                                </span>
                            @endif
						</div>
						<div class="form-group col-xs-12 col-sm-6 col-sm-offset-3{{ $errors->has('companyAddress') ? ' has-error' : '' }}">
							<label class="control-label">
								<i class="fa fa-address-book" aria-hidden="true"></i> Company Address
							</label>
							<input type="hidden" name="companyAddressId" value="{{$companyAddress->id}}">
							<input class="form-control" type="text" name="companyAddress" value="{{ $companyAddress->setting_value }}">

							@if ($errors->has('companyAddress'))
                  <span class="help-block">
                      <strong>{{ $errors->first('companyAddress') }}</strong>
                  </span>
              @endif
						</div>
            <div class="form-group col-xs-12 col-sm-6 col-sm-offset-3">
              <fieldset class="gllpLatlonPicker">
                <input type="hidden" name="companyLatLonId" value="{{$companyLatLon->id}}" />
                @php
                  $latlon = explode(',',$companyLatLon->setting_value);
                  $lat = $latlon[0];
                  $lon = $latlon[1];
                @endphp
                <input type="text" class="gllpSearchField form-control" placeholder="Search for place" style="width:75%;float:left">
                <input type="button" class="gllpSearchButton btn btn-primary" value="search" style="float:left">
              	<div class="gllpMap" style="width:100%;height:500px;">Google Maps</div>
              	<input type="readonly" name="companyLat" class="gllpLatitude form-control" value="{{$lat}}" style="width:50%;float:left"/>
              	<input type="readonly" name="companyLon" class="gllpLongitude form-control" value="{{$lon}}" style="width:50%;float:left"/>
              	<input type="hidden" class="gllpZoom" value="15"/>
              </fieldset>
            </div>
					</div>
					<footer class="panel-footer text-center">
						<button type="submit" class="btn btn-primary">Update</button>
					</footer>
		    	</section>
		    </form>
	    	</div>

	    	<div class="col-xs-12">
  	    	<form method="POST" action="{{route('saveSocmed')}}">
  	    	{{ csrf_field() }}
  		    	<section id="socmed" class="panel panel-primary">
  					<header class="panel-heading">
  						<h2>Social Media Links</h2>
  					</header>
  					<div class="panel-body">
  						@if(Session::has('message-socmed'))
  							<div class="alert {{ Session::get('alert-class') }} alert-dismissible" role="alert">
  								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  							 	<p>{{ Session::get('message-socmed') }}</p>
  							</div>
  						@endif
  						<div class="form-group col-xs-12 col-sm-4{{ $errors->has('companyFacebook') ? ' has-error' : '' }}">
  							<label class="control-label">
  								<i class="fa fa-facebook-official" aria-hidden="true"></i> Facebook
  							</label>
  							<input type="hidden" name="companyFacebookId" value="{{$companyFacebook->id}}">
  							<input class="form-control" type="text" name="companyFacebook" value="{{ $companyFacebook->setting_value }}" placeholder="Insert link...">

  							@if ($errors->has('companyFacebook'))
                    <span class="help-block">
                        <strong>{{ $errors->first('companyFacebook') }}</strong>
                    </span>
                @endif
  						</div>
  						<div class="form-group col-xs-12 col-sm-4{{ $errors->has('companyInstagram') ? ' has-error' : '' }}">
  							<label class="control-label">
  								<i class="fa fa-instagram" aria-hidden="true"></i> Instagram
  							</label>
  							<input type="hidden" name="companyInstagramId" value="{{$companyInstagram->id}}">
  							<input class="form-control" type="text" name="companyInstagram" value="{{ $companyInstagram->setting_value }}" placeholder="Insert link...">

  							@if ($errors->has('companyInstagram'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('companyInstagram') }}</strong>
                                  </span>
                              @endif
  						</div>
  						<div class="form-group col-xs-12 col-sm-4{{ $errors->has('companyTwitter') ? ' has-error' : '' }}">
  							<label class="control-label">
  								<i class="fa fa-twitter-square" aria-hidden="true"></i> Twitter
  							</label>
  							<input type="hidden" name="companyTwitterId" value="{{$companyTwitter->id}}">
  							<input class="form-control" type="text" name="companyTwitter" value="{{ $companyTwitter->setting_value }}" placeholder="Insert link...">

  							@if ($errors->has('companyTwitter'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('companyTwitter') }}</strong>
                                  </span>
                              @endif
  						</div>
              <div class="form-group col-xs-12 col-sm-4{{ $errors->has('companyPinterest') ? ' has-error' : '' }}">
  							<label class="control-label">
  								<i class="fa fa-pinterest" aria-hidden="true"></i> Pinterest
  							</label>
  							<input type="hidden" name="companyPinterestId" value="{{$companyPinterest->id}}">
  							<input class="form-control" type="text" name="companyPinterest" value="{{ $companyPinterest->setting_value }}" placeholder="Insert link...">

  							@if ($errors->has('companyPinterest'))
                    <span class="help-block">
                        <strong>{{ $errors->first('companyPinterest') }}</strong>
                    </span>
                @endif
  						</div>
              <div class="form-group col-xs-12 col-sm-4{{ $errors->has('companyPath') ? ' has-error' : '' }}">
  							<label class="control-label">
  								<i class="fa fa-pinterest-square" aria-hidden="true"></i> Path
  							</label>
  							<input type="hidden" name="companyPathId" value="{{$companyPath->id}}">
  							<input class="form-control" type="text" name="companyPath" value="{{ $companyPath->setting_value }}" placeholder="Insert link...">

  							@if ($errors->has('companyPath'))
                    <span class="help-block">
                        <strong>{{ $errors->first('companyPath') }}</strong>
                    </span>
                @endif
  						</div>
              <div class="form-group col-xs-12 col-sm-4{{ $errors->has('companyGplus') ? ' has-error' : '' }}">
  							<label class="control-label">
  								<i class="fa fa-google-plus-official" aria-hidden="true"></i> Google+
  							</label>
  							<input type="hidden" name="companyGplusId" value="{{$companyGplus->id}}">
  							<input class="form-control" type="text" name="companyGplus" value="{{ $companyGplus->setting_value }}" placeholder="Insert link...">

  							@if ($errors->has('companyGplus'))
                    <span class="help-block">
                        <strong>{{ $errors->first('companyGplus') }}</strong>
                    </span>
                @endif
  						</div>
              <div class="form-group col-xs-12 col-sm-4{{ $errors->has('companyLinkedIn') ? ' has-error' : '' }}">
  							<label class="control-label">
  								<i class="fa fa-linkedin-square" aria-hidden="true"></i> LinkedIn
  							</label>
  							<input type="hidden" name="companyLinkedInId" value="{{$companyLinkedIn->id}}">
  							<input class="form-control" type="text" name="companyLinkedIn" value="{{ $companyLinkedIn->setting_value }}" placeholder="Insert link...">

  							@if ($errors->has('companyLinkedIn'))
                    <span class="help-block">
                        <strong>{{ $errors->first('companyLinkedIn') }}</strong>
                    </span>
                @endif
  						</div>
              <div class="form-group col-xs-12 col-sm-4{{ $errors->has('companySnapchat') ? ' has-error' : '' }}">
  							<label class="control-label">
  								<i class="fa fa-snapchat-square" aria-hidden="true"></i> Snapchat
  							</label>
  							<input type="hidden" name="companySnapchatId" value="{{$companySnapchat->id}}">
  							<input class="form-control" type="text" name="companySnapchat" value="{{ $companySnapchat->setting_value }}" placeholder="Insert link...">

  							@if ($errors->has('companySnapchat'))
                    <span class="help-block">
                        <strong>{{ $errors->first('companySnapchat') }}</strong>
                    </span>
                @endif
  						</div>
  					</div>
  					<footer class="panel-footer text-right">
  						<button type="submit" class="btn btn-primary">Update</button>
  					</footer>
  		    	</section>
  		    </form>
	    	</div>
	    </div>
	    {{-- MESSAGES --}}
	    <div class="row">
		    <div class="col-xs-12">
			    <div class="panel panel-primary">
			    	<div class="panel-heading">
			    		<h2>Messages</h2>
			    	</div>
			    	<div id="messages" class="panel-body">
				    	@if(Session::has('message-messages'))
							<div class="alert {{ Session::get('alert-class') }} alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							 	<p>{{ Session::get('message-messages') }}</p>
							</div>
						@endif
				    	<div class="table-responsive">
				    		<table id="messagesTable" class="table table-hover">
							    <thead>
								    <tr>
								    	<th class="text-center">#</th>
								        <th>Name</th>
								        <th>E-Mail</th>
								        <th>Message</th>
								        <th>Date</th>
								        <th class="text-center">Options</th>
								    </tr>
							    </thead>
							    <tbody>
								    @foreach($messages as $key => $val)
									    <tr class="{{ ($val->message_status == 1)?'unread':'' }}">
									    	<td class="text-center">{{ $key+1 }}</td>
									        <td>{{ $val->message_sender_name }}</td>
									        <td>{{ $val->message_sender_email }}</td>
									        <td>{{ substr($val->message_sender_content,0,25) }} ...</td>
									        <td>{{ date('d/m/Y',strtotime($val->created_at)) }}</td>
									        <td class="text-center">
									        	<button class="btn btn-warning" title="Read Message" data-toggle="modal" data-target="#read{{ $val->id }}">
									        		@if($val->message_status == "1")
									        			<i class="fa fa-envelope" aria-hidden="true"></i>
									        		@else
									        			<i class="fa fa-envelope-open-o" aria-hidden="true"></i>
									        		@endif
									        	</button>
									        	<div id="read{{ $val->id }}" class="modal fade modal-message" role="dialog">
													<div class="modal-dialog">

														<div class="modal-content text-left">
															<header class="modal-header">
																From <strong>{{ $val->message_sender_name }}</strong> (<strong>{{ $val->message_sender_email }}</strong>)
															</header>
															<div class="modal-body">
																<small>{{ date('d/m/Y',strtotime($val->created_at)) }}</small>
																<div class="message-content">
																	{{ $val->message_sender_content }}
																</div>
															</div>
															<div class="modal-footer">
																<a href="mailto:{{$val->message_sender_email}}" class="btn btn-primary">Reply</a>
																<button class="btn btn-default read-message" data-dismiss="modal" data-id="{{ $val->id }}">Cancel</button>
															</div>
														</div>

													</div>
												</div>

												<button title="Delete message" class="btn btn-danger" data-toggle="modal" data-target="#delete{{ $val->id }}"><i class="fa fa-trash" aria-hidden="true"></i></button>
												<div id="delete{{ $val->id }}" class="modal fade" role="dialog">
													<div class="modal-dialog modal-sm">

													<form class="" method="post" action="{{ url('backend/contact/delete_message/'.$val->id) }}">
														{{ method_field('DELETE') }}
														{{ csrf_field() }}
														<input name="id" value="{{ $val->id }}" type="hidden">
														<div class="modal-content text-center">
															<div class="modal-body">
																<h3>Delete message from <strong>'{{ $val->message_sender_name }}'</strong> ?</h3>

																<button type="submit" class="btn btn-primary">Delete</button>
															<button class="btn btn-default" data-dismiss="modal">Close</button>
															</div>
														</div>
													</form>

													</div>
												</div>
									        </td>
									    </tr>
									@endforeach
							    </tbody>
							</table>
				    	</div>
			    	</div>
			    </div>
		    </div>
		</div>

    </div>
@endsection

@section('script')
<script type="text/javascript">
	$("#messagesTable").DataTable();

	$(".read-message").click(function(){
		$.ajax({
	        type: 'post',
	        url: '{{ url("/backend/contact/read_message") }}',
	        data: {
	            '_token': '{{ csrf_token() }}',
	            'id': $(this).data('id'),
	        },
	        success: function(data) {
	            location.reload();
	        },
	    });
	});
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA-jnR5E42N3pmnms7ZWeRAlkAkA4gJYCE"></script>
<script src="{{asset($asset_url.'js/backend/jquery-gmaps-latlon-picker.js')}}"></script>
@endsection
