@extends('layouts.masterBackend')

@section('title')
    {{ ucfirst('Home') }}
@endsection

@section('content')
    <!-- MAIN CONTENT -->
    <div class="container-fluid">
    	<div class="row">
    		<div class="col-xs-12">
    			<section id="message-carousel" class="panel panel-primary">
  					<header class="panel-heading">
  						<h2>
  							Carousel
  							<button class="btn btn-success" data-toggle="modal" data-target="#addCarousel">Add Image</button>
                <button class="btn btn-warning" data-toggle="modal" data-target="#reorder">Arrange order</button>
  						</h2>
  					</header>
  					<div class="panel-body">
  						@if(Session::has('message-carousel'))
  							<div class="alert {{ Session::get('alert-class') }} alert-dismissible" role="alert">
  								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  							 	<p>{{ Session::get('message-carousel') }}</p>
  							</div>
  						@endif
  						<div class="table-responsive">
  			    		<table id="carouselTable" class="table table-hover">
  						    <thead>
  							    <tr>
  							    	<th class="text-center">#</th>
  							        <th class="text-center">Image</th>
                        <th class="text-center">Caption</th>
  							        <th class="text-center">Options</th>
  							    </tr>
  						    </thead>
  						    <tbody>
  							    @foreach($carousel as $key => $val)
  								    <tr>
  								    	<td class="text-center">{{$key+1}}</td>
  								        <td class="text-center">
  								        	<img class="img img-responsive" alt="image" src="{{ asset($storage_url.'img/home/'.$val->media_url) }}" style="max-height: 100px;margin: 0 auto">
  								        </td>
                          <td class="text-center">
                            {{ $val->media_title }}
                          </td>
  								        <td class="text-center">
                            {{-- Update data --}}
                            <button title="Update Image" class="btn btn-warning" data-toggle="modal" data-target="#update{{ $val->id }}"><i class="fa fa-pencil" aria-hidden="true"></i></button>

  								        	{{-- Delete data --}}
    												<button title="Delete Image" class="btn btn-danger" data-toggle="modal" data-target="#delete{{ $val->id }}"><i class="fa fa-trash" aria-hidden="true"></i></button>
    												<div id="delete{{ $val->id }}" class="modal fade" role="dialog">
    													<div class="modal-dialog modal-sm">

    													{!! Form::open(['url' => url('backend/home/delete_carousel/'.$val->id),'method'=>'delete']) !!}
    														{!! Form::hidden('id',$val->id) !!}
    														<div class="modal-content text-center">
    															<div class="modal-body">
    																<h3>Delete image ?</h3>
    																<img class="img img-responsive" alt="image" src="{{ asset($storage_url.'img/home/'.$val->media_url) }}">
    																<br>
    																<button type="submit" class="btn btn-primary">Delete</button>
    															<button class="btn btn-default" data-dismiss="modal">Cancel</button>
    															</div>
    														</div>
    													{!! Form::close() !!}

    													</div>
    												</div>
  								      </td>
  								    </tr>
  								  @endforeach
  						    </tbody>
  						  </table>
  			    	</div>
  					</div>
		    	</section>

          @foreach ($carousel as $key => $val)
            <div id="update{{$val->id}}" class="modal fade" role="dialog">
              <div class="modal-dialog">

              {!! Form::open(['url' => url('backend/home/save_carousel'),'method'=>'post','files'=>true]) !!}
                {!! Form::hidden('carousel_id',$val->id) !!}
                <div class="modal-content">
                  <div class="modal-header">
                    <h2>Update Image</h2>
                  </div>
                  <div class="modal-body text-center">
                    <div class="form-group image-with-preview">
                      <img class="prev-img img-bordered img-responsive" alt="banner" src="{{ asset($storage_url.'img/home/'.$val->media_url) }}">
                      {!! Form::label('imagefile','Choose an image',['class'=>'control-label']) !!}
                      <div class="{{ $errors->has('imagefile') ? ' has-error' : '' }}">
                        {!! Form::file('imagefile',['class'=>'form-control','data-fileinput'=>'image']) !!}
                        @if ($errors->has('imagefile'))
                            <span class="help-block">
                                <strong>{{ $errors->first('imagefile') }}</strong>
                            </span>
                        @endif
                      </div>
                      <br>
                      <div class="upload-rules text-left">
                        <ul>
                          <li>Min resolution <strong>1600 x 575 px</strong></li>
                          <li>File type <strong>*.jpg / *.jpeg</strong></li>
                          <li>Max file size <strong>1mb</strong></li>
                        </ul>
                      </div>
                    </div>
                    <div class="form-group">
                      {!! Form::label('imagetitle','Image Title',['class'=>'control-label']) !!}
                      <div class="{{ $errors->has('imagetitle') ? ' has-error' : '' }}">
                        {!! Form::text('imagetitle',$val->media_title,['class'=>'form-control']) !!}
                        @if ($errors->has('imagetitle'))
                            <span class="help-block">
                                <strong>{{ $errors->first('imagetitle') }}</strong>
                            </span>
                        @endif
                      </div>
                    </div>
                    <div class="form-group">
                      {!! Form::label('imagealt','Image Alt',['class'=>'control-label']) !!}
                      <div class="{{ $errors->has('imagealt') ? ' has-error' : '' }}">
                        {!! Form::text('imagealt',$val->media_content,['class'=>'form-control']) !!}
                        @if ($errors->has('imagealt'))
                            <span class="help-block">
                                <strong>{{ $errors->first('imagealt') }}</strong>
                            </span>
                        @endif
                      </div>
                    </div>
                  </div>
                  <footer class="modal-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <button class="btn btn-default" data-dismiss="modal">Cancel</button>
                  </footer>
                </div>
              {!! Form::close() !!}

              </div>
            </div>
          @endforeach

		    	{{-- ADD CAROUSEL MODAL --}}
		    	<div id="addCarousel" class="modal fade" role="dialog">
  					<div class="modal-dialog">

    				  {!! Form::open(['url' => url('backend/home/save_carousel'),'method'=>'post','files'=>true]) !!}
    						{!! Form::hidden('id','1') !!}
    						<div class="modal-content">
    							<div class="modal-header">
    								<h2>Add Image</h2>
    							</div>
    							<div class="modal-body text-center">
    								<div class="form-group image-with-preview">
    									<img class="prev-img img-bordered img-responsive" alt="banner" src="{{ asset($storage_url.'img/img4x3.jpg') }}">
    									{!! Form::label('imagefile','Choose an image',['class'=>'control-label']) !!}
    									<div class="{{ $errors->has('imagefile') ? ' has-error' : '' }}">
    										{!! Form::file('imagefile',['class'=>'form-control','data-fileinput'=>'image']) !!}
    										@if ($errors->has('imagefile'))
                            <span class="help-block">
                                <strong>{{ $errors->first('imagefile') }}</strong>
                            </span>
                        @endif
                      </div>
    									<br>
    									<div class="upload-rules text-left">
    										<ul>
    											<li>Min resolution <strong>1600 x 575 px</strong></li>
    											<li>File type <strong>*.jpg / *.jpeg</strong></li>
    											<li>Max file size <strong>1mb</strong></li>
    										</ul>
    									</div>
    								</div>
                    <div class="form-group">
                      {!! Form::label('imagetitle','Image Title',['class'=>'control-label']) !!}
    									<div class="{{ $errors->has('imagetitle') ? ' has-error' : '' }}">
    										{!! Form::text('imagetitle','',['class'=>'form-control']) !!}
    										@if ($errors->has('imagetitle'))
                            <span class="help-block">
                                <strong>{{ $errors->first('imagetitle') }}</strong>
                            </span>
                        @endif
                      </div>
                    </div>
                    <div class="form-group">
                      {!! Form::label('imagealt','Image Alt',['class'=>'control-label']) !!}
    									<div class="{{ $errors->has('imagealt') ? ' has-error' : '' }}">
    										{!! Form::text('imagealt','',['class'=>'form-control']) !!}
    										@if ($errors->has('imagealt'))
                            <span class="help-block">
                                <strong>{{ $errors->first('imagealt') }}</strong>
                            </span>
                        @endif
                      </div>
                    </div>
    							</div>
    							<footer class="modal-footer">
    								<button type="submit" class="btn btn-primary">Save</button>
    								<button class="btn btn-default" data-dismiss="modal">Cancel</button>
    							</footer>
    						</div>
    					{!! Form::close() !!}

  					</div>
  				</div>

          {{-- reorder --}}
          <div id="reorder" class="modal fade" role="dialog">
            <div class="modal-dialog">

              {!! Form::open(['url' => url('backend/home/order_carousel'),'method'=>'post']) !!}
                <div class="modal-content">
                  <div class="modal-header">
                    <h2>Rearrange order</h2>
                  </div>
                  <div class="modal-body text-center">
                    @foreach ($carousel as $x => $vals)
                    <div class="form-group text-left">
                      <div class="row">
                        <div class="col-xs-12 col-sm-1">
                          {!! Form::label('reorder',$x+1,['class'=>'control-label']) !!}
                        </div>
                        <div class="col-xs-12 col-sm-11">
                          <select name="reorder[]" class="image-select form-control">
                              @foreach ($carousel as $key => $val)
                              <option
                                  value="{{ $val->id }}"
                                  data-img-src="{{ asset($storage_url.'img/home/'.$val->media_url) }}"
                                  @if ($x == $key)
                                    selected="selected"
                                  @endif
                              >
                                  ( {{ $key+1 }} ) {{ $val->media_title }}
                              </option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                    @endforeach
                  </div>
                  <footer class="modal-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <button class="btn btn-default" data-dismiss="modal">Cancel</button>
                  </footer>
                </div>
              {!! Form::close() !!}

            </div>
          </div>
    		</div>

        <div class="col-xs-12">
        <section id="message-home-content" class="panel panel-primary">
        <header class="panel-heading">
          <h2>
            Introduction content
          </h2>
        </header>
        {!! Form::open(['url' => url('backend/home/save_content'),'method'=>'post']) !!}
          {!! Form::hidden('id',isset($content->id)?$content->id:'') !!}
        <div class="panel-body">
          @if(Session::has('message-home-content'))
            <div class="alert {{ Session::get('alert-class') }} alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <p>{{ Session::get('message-home-content') }}</p>
            </div>
          @endif

          <div class="col-xs-12 col-sm-8 col-sm-offset-2">
            <div class="form-group">
              {!! Form::label('title','Title',['class'=>'control-label']) !!}
              <div class="{{ $errors->has('title') ? ' has-error' : '' }}">
                {!! Form::text('title',isset($content->general_title)?$content->general_title:'',['class'=>'form-control']) !!}
                @if ($errors->has('title'))
                    <span class="help-block">
                        <strong>{{ $errors->first('title') }}</strong>
                    </span>
                @endif
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-8 col-sm-offset-2">
            <div class="form-group">
              {!! Form::label('text','Text',['class'=>'control-label']) !!}
              <div class="{{ $errors->has('text') ? ' has-error' : '' }}">
                {!! Form::textarea('text',isset($content->general_content)?$content->general_content:'',['class'=>'form-control']) !!}
                @if ($errors->has('text'))
                    <span class="help-block">
                        <strong>{{ $errors->first('text') }}</strong>
                    </span>
                @endif
              </div>
            </div>
          </div>
        </div>
        <footer class="panel-footer text-center">
          <button type="submit" class="btn btn-primary">Update</button>
        </footer>
        {!! Form::close() !!}
        </section>
      </div>

        <div class="col-xs-12">
        <section id="message-card" class="panel panel-primary">
          <header class="panel-heading">
            <h2>
              Cards
              @if(count($cards) < 3)
              <button class="btn btn-success" data-toggle="modal" data-target="#addCard">Add Card</button>
              @endif
              <button class="btn btn-warning" data-toggle="modal" data-target="#reorderCard">Arrange order</button>
            </h2>
          </header>
          <div class="panel-body">
            @if(Session::has('message-card'))
              <div class="alert {{ Session::get('alert-class') }} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <p>{{ Session::get('message-card') }}</p>
              </div>
            @endif
            <div class="table-responsive">
              <table id="cardTable" class="table table-hover">
                <thead>
                  <tr>
                    <th class="text-center">#</th>
                      <th class="text-center">Image</th>
                      <th class="text-center">Caption</th>
                      <th class="text-center">Options</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($cards as $key => $val)
                    <tr>
                      <td class="text-center">{{$key+1}}</td>
                        <td class="text-center">
                          <img class="img img-responsive" alt="image" src="{{ asset($storage_url.'img/home/'.$val->media_url) }}" style="max-height: 100px;margin: 0 auto">
                        </td>
                        <td class="text-center">
                          {{ $val->media_title }}
                        </td>
                        <td class="text-center">
                          {{-- Update data --}}
                          <button title="Update Card" class="btn btn-warning" data-toggle="modal" data-target="#updatecard{{ $val->id }}"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                          <div id="updatecard{{$val->id}}" class="modal fade" role="dialog">
                            <div class="modal-dialog">

                            {!! Form::open(['url' => url('backend/home/save_card'),'method'=>'post','files'=>true]) !!}
                              {!! Form::hidden('card_id',$val->id) !!}
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h2>Update Card</h2>
                                </div>
                                <div class="modal-body text-center">
                                  <div class="form-group image-with-preview">
                                    <img class="prev-img img-bordered img-responsive" alt="card image" src="{{ asset($storage_url.'img/home/'.$val->media_url) }}">
                                    {!! Form::label('imagefilecard','Choose an image',['class'=>'control-label']) !!}
                                    <div class="{{ $errors->has('imagefilecard') ? ' has-error' : '' }}">
                                      {!! Form::file('imagefilecard',['class'=>'form-control','data-fileinput'=>'image']) !!}
                                      @if ($errors->has('imagefilecard'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('imagefilecard') }}</strong>
                                          </span>
                                      @endif
                                    </div>
                                    <br>
                                    <div class="upload-rules text-left">
                                      <ul>
                                        <li>Min resolution <strong>300 x 300 px</strong></li>
                                        <li>File type <strong>*.jpg / *.jpeg</strong></li>
                                        <li>Max file size <strong>1mb</strong></li>
                                      </ul>
                                    </div>
                                  </div>

                                  <div class="form-group">
                                    {!! Form::label('imagealt','Image Alt',['class'=>'control-label']) !!}
                                    <div class="{{ $errors->has('imagealt') ? ' has-error' : '' }}">
                                      {!! Form::text('imagealt',$val->media_desc,['class'=>'form-control']) !!}
                                      @if ($errors->has('imagealt'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('imagealt') }}</strong>
                                          </span>
                                      @endif
                                    </div>
                                  </div>

                                  <div class="form-group">
                                    {!! Form::label('cardtitle','Card Title',['class'=>'control-label']) !!}
                                    <div class="{{ $errors->has('cardtitle') ? ' has-error' : '' }}">
                                      {!! Form::text('cardtitle',$val->media_title,['class'=>'form-control']) !!}
                                      @if ($errors->has('cardtitle'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('cardtitle') }}</strong>
                                          </span>
                                      @endif
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    {!! Form::label('cardcontent','Card Content',['class'=>'control-label']) !!}
                                    <div class="{{ $errors->has('cardcontent') ? ' has-error' : '' }}">
                                      {!! Form::textarea('cardcontent',$val->media_content,['class'=>'form-control']) !!}
                                      @if ($errors->has('cardcontent'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('cardcontent') }}</strong>
                                        </span>
                                      @endif
                                    </div>
                                  </div>
                                </div>
                                <footer class="modal-footer">
                                  <button type="submit" class="btn btn-primary">Save</button>
                                  <button class="btn btn-default" data-dismiss="modal">Cancel</button>
                                </footer>
                              </div>
                            {!! Form::close() !!}

                            </div>
                          </div>
                          {{-- Delete data --}}
                          <button title="Delete Card" class="btn btn-danger" data-toggle="modal" data-target="#deletecard{{ $val->id }}"><i class="fa fa-trash" aria-hidden="true"></i></button>
                          <div id="deletecard{{ $val->id }}" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-sm">

                            {!! Form::open(['url' => url('backend/home/delete_card/'.$val->id),'method'=>'delete']) !!}
                              {!! Form::hidden('id',$val->id) !!}
                              <div class="modal-content text-center">
                                <div class="modal-body">
                                  <h3>Delete Card ?</h3>
                                  <img class="img img-responsive" alt="image" src="{{ asset($storage_url.'img/home/'.$val->media_url) }}">
                                  <br>
                                  <button type="submit" class="btn btn-primary">Delete</button>
                                <button class="btn btn-default" data-dismiss="modal">Cancel</button>
                                </div>
                              </div>
                            {!! Form::close() !!}

                            </div>
                          </div>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </section>

        {{-- ADD CARD MODAL --}}
        @if(count($cards) < 3)
        <div id="addCard" class="modal fade" role="dialog">
          <div class="modal-dialog">

            {!! Form::open(['url' => url('backend/home/save_card'),'method'=>'post','files'=>true]) !!}
              <div class="modal-content">
                <div class="modal-header">
                  <h2>Add Card</h2>
                </div>
                <div class="modal-body text-center">
                  <div class="form-group image-with-preview">
                    <img class="prev-img img-bordered img-responsive" alt="card image" src="{{ asset($storage_url.'img/img4x3.jpg') }}">
                    {!! Form::label('imagefilecard','Choose an image',['class'=>'control-label']) !!}
                    <div class="{{ $errors->has('imagefilecard') ? ' has-error' : '' }}">
                      {!! Form::file('imagefilecard',['class'=>'form-control','data-fileinput'=>'image']) !!}
                      @if ($errors->has('imagefilecard'))
                          <span class="help-block">
                              <strong>{{ $errors->first('imagefilecard') }}</strong>
                          </span>
                      @endif
                    </div>
                    <br>
                    <div class="upload-rules text-left">
                      <ul>
                        <li>Min resolution <strong>300 x 300 px</strong></li>
                        <li>File type <strong>*.jpg / *.jpeg</strong></li>
                        <li>Max file size <strong>1mb</strong></li>
                      </ul>
                    </div>
                  </div>

                  <div class="form-group">
                    {!! Form::label('imagealt','Image Alt',['class'=>'control-label']) !!}
                    <div class="{{ $errors->has('imagealt') ? ' has-error' : '' }}">
                      {!! Form::text('imagealt','',['class'=>'form-control']) !!}
                      @if ($errors->has('imagealt'))
                          <span class="help-block">
                              <strong>{{ $errors->first('imagealt') }}</strong>
                          </span>
                      @endif
                    </div>
                  </div>

                  <div class="form-group">
                    {!! Form::label('cardtitle','Card Title',['class'=>'control-label']) !!}
                    <div class="{{ $errors->has('cardtitle') ? ' has-error' : '' }}">
                      {!! Form::text('cardtitle','',['class'=>'form-control']) !!}
                      @if ($errors->has('cardtitle'))
                          <span class="help-block">
                              <strong>{{ $errors->first('cardtitle') }}</strong>
                          </span>
                      @endif
                    </div>
                  </div>
                  <div class="form-group">
                    {!! Form::label('cardcontent','Card Content',['class'=>'control-label']) !!}
                    <div class="{{ $errors->has('cardcontent') ? ' has-error' : '' }}">
                      {!! Form::textarea('cardcontent','',['class'=>'form-control']) !!}
                      @if ($errors->has('cardcontent'))
                        <span class="help-block">
                            <strong>{{ $errors->first('cardcontent') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>
                </div>
                <footer class="modal-footer">
                  <button type="submit" class="btn btn-primary">Save</button>
                  <button class="btn btn-default" data-dismiss="modal">Cancel</button>
                </footer>
              </div>
            {!! Form::close() !!}

          </div>
        </div>
        @endif

        {{-- reorder --}}
        <div id="reorderCard" class="modal fade" role="dialog">
          <div class="modal-dialog">

            {!! Form::open(['url' => url('backend/home/order_card'),'method'=>'post']) !!}
              <div class="modal-content">
                <div class="modal-header">
                  <h2>Rearrange order</h2>
                </div>
                <div class="modal-body text-center">
                  @foreach ($cards as $x => $vals)
                  <div class="form-group text-left">
                    <div class="row">
                      <div class="col-xs-12 col-sm-1">
                        {!! Form::label('reorder',$x+1,['class'=>'control-label']) !!}
                      </div>
                      <div class="col-xs-12 col-sm-11">
                        <select name="reorder[]" class="image-select form-control">
                            @foreach ($cards as $key => $val)
                            <option
                                value="{{ $val->id }}"
                                data-img-src="{{ asset($storage_url.'img/home/'.$val->media_url) }}"
                                @if ($x == $key)
                                  selected="selected"
                                @endif
                            >
                                ( {{ $key+1 }} ) {{ $val->media_title }}
                            </option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                  </div>
                  @endforeach
                </div>
                <footer class="modal-footer">
                  <button type="submit" class="btn btn-primary">Save</button>
                  <button class="btn btn-default" data-dismiss="modal">Cancel</button>
                </footer>
              </div>
            {!! Form::close() !!}

          </div>
        </div>
      </div>

    		<div class="col-xs-12">
    			<section id="message-divider" class="panel panel-primary">
					<header class="panel-heading">
						<h2>
							Divider
						</h2>
					</header>
					{!! Form::open(['url' => url('backend/home/save_divider'),'method'=>'post']) !!}
					<div class="panel-body">
						@if(Session::has('message-divider'))
							<div class="alert {{ Session::get('alert-class') }} alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							 	<p>{{ Session::get('message-divider') }}</p>
							</div>
						@endif

						<div class="col-xs-12 col-sm-6">
							<div class="form-group">
								{!! Form::label('divider1','Divider 1',['class'=>'control-label']) !!}
								<div class="{{ $errors->has('divider1') ? ' has-error' : '' }}">
									{!! Form::hidden('id1',$divider1->id) !!}
									{!! Form::text('divider1',$divider1->general_content,['class'=>'form-control']) !!}
									@if ($errors->has('divider1'))
	                                    <span class="help-block">
	                                        <strong>{{ $errors->first('divider1') }}</strong>
	                                    </span>
	                                @endif
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6">
							<div class="form-group">
								{!! Form::label('divider2','Divider 2',['class'=>'control-label']) !!}
								<div class="{{ $errors->has('divider2') ? ' has-error' : '' }}">
									{!! Form::hidden('id2', $divider2->id) !!}
									{!! Form::text('divider2',$divider2->general_content,['class'=>'form-control']) !!}
									@if ($errors->has('divider2'))
	                                    <span class="help-block">
	                                        <strong>{{ $errors->first('divider2') }}</strong>
	                                    </span>
	                                @endif
								</div>
							</div>
						</div>
					</div>
					<footer class="panel-footer text-center">
						<button type="submit" class="btn btn-primary">Update</button>
					</footer>
					{!! Form::close() !!}
		    	</section>
    		</div>

    		<div class="col-xs-12">
    			<section class="panel panel-primary">
					<header class="panel-heading">
						<h2>
							Banner
						</h2>
					</header>

					<div class="panel-body">
						{!! Form::open(['url' => url('backend/home/save_banner'),'method'=>'post','files'=>true]) !!}
							{!! Form::hidden('bannerid',$banner1->id) !!}
							{!! Form::hidden('bannertype','banner1') !!}
							<div id="message-banner1" class="panel panel-primary">
								<div class="panel-heading">
									<h3>Banner 1</h3>
								</div>
								<div class="panel-body">
									@if(Session::has('message-banner1'))
										<div class="alert {{ Session::get('alert-class') }} alert-dismissible" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										 	<p>{{ Session::get('message-banner1') }}</p>
										</div>
									@endif

									<div class="col-xs-12 col-sm-6">
										<div class="form-group image-with-preview">
											<img class="prev-img img-bordered img-responsive" alt="banner" src="{{ isset($banner1->media_url)?asset($storage_url.'img/home/'.$banner1->media_url):asset($storage_url.'img/img4x3.jpg') }}">
											{!! Form::label('image_banner1','Choose an Image',['class'=>'control-label']) !!}
											<div class="{{ $errors->has('image_banner1') ? ' has-error' : '' }}">
												{!! Form::file('image_banner1',['class'=>'form-control','data-fileinput'=>'image']) !!}
												@if ($errors->has('image_banner1'))
				                                    <span class="help-block">
				                                        <strong>{{ $errors->first('image_banner1') }}</strong>
				                                    </span>
				                                @endif
				                                <br>
												<div class="upload-rules text-left">
													<ul>
														<li>Min resolution <strong>1600 x 550 px</strong></li>
														<li>File type <strong>*.jpg / *.jpeg</strong></li>
														<li>Max file size <strong>1mb</strong></li>
													</ul>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-6">
										<div class="form-group">
											{!! Form::label('title_banner1','Title Banner 1',['class'=>'control-label']) !!}
											<div class="{{ $errors->has('title_banner1') ? ' has-error' : '' }}">
												{!! Form::text('title_banner1',$banner1->media_title,['class'=>'form-control']) !!}
												@if ($errors->has('title_banner1'))
				                                    <span class="help-block">
				                                        <strong>{{ $errors->first('title_banner1') }}</strong>
				                                    </span>
				                                @endif
											</div>
										</div>
										<div class="form-group">
											{!! Form::label('content_banner1','Content Banner 1',['class'=>'control-label']) !!}
											<div class="{{ $errors->has('content_banner1') ? ' has-error' : '' }}">
												{!! Form::textarea('content_banner1',$banner1->media_content,['class'=>'form-control']) !!}
												@if ($errors->has('content_banner1'))
				                                    <span class="help-block">
				                                        <strong>{{ $errors->first('content_banner1') }}</strong>
				                                    </span>
				                                @endif
											</div>
										</div>
									</div>
								</div>
								<footer class="panel-footer text-center">
									<button type="submit" class="btn btn-primary">Update</button>
								</footer>
							</div>
						{!! Form::close() !!}
						{!! Form::open(['url' => url('backend/home/save_banner'),'method'=>'post','files'=>true]) !!}
							{!! Form::hidden('bannerid',$banner2->id) !!}
							{!! Form::hidden('bannertype','banner2') !!}
							<div id="message-banner2" class="panel panel-primary">
								<div class="panel-heading">
									<h3>Banner 2</h3>
								</div>
								<div class="panel-body">
									@if(Session::has('message-banner2'))
										<div class="alert {{ Session::get('alert-class') }} alert-dismissible" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										 	<p>{{ Session::get('message-banner2') }}</p>
										</div>
									@endif

									<div class="col-xs-12 col-sm-6">
										<div class="form-group image-with-preview">
											<img class="prev-img img-bordered img-responsive" alt="banner" src="{{ isset($banner2->media_url)?asset($storage_url.'img/home/'.$banner2->media_url):asset($storage_url.'img/img4x3.jpg') }}">
											{!! Form::label('image_banner2','Choose an Image',['class'=>'control-label']) !!}
											<div class="{{ $errors->has('image_banner2') ? ' has-error' : '' }}">
												{!! Form::file('image_banner2',['class'=>'form-control','data-fileinput'=>'image']) !!}
												@if ($errors->has('image_banner2'))
				                                    <span class="help-block">
				                                        <strong>{{ $errors->first('image_banner2') }}</strong>
				                                    </span>
				                                @endif
				                                <br>
												<div class="upload-rules text-left">
													<ul>
														<li>Min resolution <strong>1600 x 550 px</strong></li>
														<li>File type <strong>*.jpg / *.jpeg</strong></li>
														<li>Max file size <strong>1mb</strong></li>
													</ul>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-6">
										<div class="form-group">
											{!! Form::label('title_banner2','Title Banner 2',['class'=>'control-label']) !!}
											<div class="{{ $errors->has('title_banner2') ? ' has-error' : '' }}">
												{!! Form::text('title_banner2',$banner2->media_title,['class'=>'form-control']) !!}
												@if ($errors->has('title_banner2'))
				                                    <span class="help-block">
				                                        <strong>{{ $errors->first('title_banner2') }}</strong>
				                                    </span>
				                                @endif
											</div>
										</div>
										<div class="form-group">
											{!! Form::label('content_banner2','Content Banner 2',['class'=>'control-label']) !!}
											<div class="{{ $errors->has('content_banner2') ? ' has-error' : '' }}">
												{!! Form::textarea('content_banner2',$banner2->media_content,['class'=>'form-control']) !!}
												@if ($errors->has('content_banner2'))
				                                    <span class="help-block">
				                                        <strong>{{ $errors->first('content_banner2') }}</strong>
				                                    </span>
				                                @endif
											</div>
										</div>
									</div>
								</div>
								<footer class="panel-footer text-center">
									<button type="submit" class="btn btn-primary">Update</button>
								</footer>
							</div>
						{!! Form::close() !!}
					</div>

		    	</section>
    		</div>
    	</div>
    </div>
@endsection

@section('script')
<script type="text/javascript">
	$("#carouselTable").DataTable();
  $("#cardTable").DataTable();

	@if ($errors->has('imagefile'))
        $("#addCarousel").modal('show');
    @endif

    @if ($errors->has('imagefilecard'))
          $("#addCard").modal('show');
      @endif

  $(".image-select").chosen();
</script>
@endsection
