@extends('layouts.masterBackend')

@section('title')
    {{ ucfirst('User Management') }}
@endsection

@section('content')

@php
    $page = 'users';
    if(Auth::user()->level != '1'){
@endphp
    <script type="text/javascript">
        window.location.href="{{ url('/backend/dashboard') }}";
    </script>
@php 
    }
@endphp
    <!-- MAIN CONTENT -->
    <div class="container-fluid">
    	<div class="row">
		    <div class="col-xs-12">
			    <div class="panel panel-primary">
			    	<div class="panel-heading">
			    		<a class="btn btn-success" href="{{ route('register') }}">Register New User</a>
			    	</div>
			    	<div class="panel-body">
				    	@if(Session::has('message'))
							<div class="alert {{ Session::get('alert-class') }} alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							 	<p>{{ Session::get('message') }}</p>
							</div>
						@endif
				    	<div class="table-responsive">
				    		<table id="usersTable" class="table table-hover">
							    <thead>
								    <tr>
								    	<th class="text-center">#</th>
								        <th>Name</th>
								        <th>E-Mail</th>
								        <th>Level</th>
								        <th class="text-center">Options</th>
								    </tr>
							    </thead>
							    <tbody>
								    @foreach($users as $key => $val)
									    <tr>
									    	<td class="text-center">{{$key+1}}</td>
									        <td>{{ $val->name }}</td>
									        <td>{{ $val->email }}</td>
									        <td>{{ ($val->level=='1')?'Administrator':'Operator' }}</td>
									        <td class="text-center">
									        	<a class="btn btn-warning" href="{{ url('/backend/user_management/update/'.$val->id) }}" title="Update data">
									        		<i class="fa fa-pencil" aria-hidden="true"></i>
									        	</a>
												
												{{-- Make admin --}}
									        	@php
									        		if($val->level != 1){
									        	@endphp
												<button title="Make {{($val->level == '1')?'Operator':'Admin'}}" class="btn btn-success" data-toggle="modal" data-target="#level{{ $val->id }}"><i class="fa fa-angle-double-up" aria-hidden="true"></i></button>
												<div id="level{{ $val->id }}" class="modal fade" role="dialog">
													<div class="modal-dialog modal-sm">

													<form class="" method="post" action="{{ route('levelUser') }}">
														{{ csrf_field() }}
														<input name="id" value="{{ $val->id }}" type="hidden">
														<input name="level" value="{{ $val->level }}" type="hidden">
														<div class="modal-content text-center">
															<div class="modal-body">
																<h3>Make <strong>'{{ $val->name }}'</strong> as {{($val->level == '1')?'Operator':'Admin'}}?</h3>

																<button type="submit" class="btn btn-primary">Make Admin</button>
															<button class="btn btn-default" data-dismiss="modal">Cancel</button>
															</div>
														</div>
													</form>

													</div>
												</div>
												@php
													}
												@endphp

									        	{{-- Delete data --}}
									        	@php
									        		if(count($users) > 1 && $val->id != Auth::user()->id){
									        	@endphp
												<button title="Delete user" class="btn btn-danger" data-toggle="modal" data-target="#delete{{ $val->id }}"><i class="fa fa-trash" aria-hidden="true"></i></button>
												<div id="delete{{ $val->id }}" class="modal fade" role="dialog">
													<div class="modal-dialog modal-sm">

													<form class="" method="post" action="{{ url('backend/user_management/delete/'.$val->id) }}">
														{{ method_field('DELETE') }}
														{{ csrf_field() }}
														<input name="id" value="{{ $val->id }}" type="hidden">
														<div class="modal-content text-center">
															<div class="modal-body">
																<h3>Delete <strong>'{{ $val->name }}'</strong> ?</h3>

																<button type="submit" class="btn btn-primary">Delete</button>
															<button class="btn btn-default" data-dismiss="modal">Cancel</button>
															</div>
														</div>
													</form>

													</div>
												</div>
												@php
													}
												@endphp
									        </td>
									    </tr>
									@endforeach
							    </tbody>
							</table>
				    	</div>
			    	</div>
			    </div>
		    </div>
		</div>
    </div>
@endsection

@section('script')
<script type="text/javascript">
	$("#usersTable").DataTable();
</script>
@endsection