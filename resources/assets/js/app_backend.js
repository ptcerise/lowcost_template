
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

+function($){
	$("input[data-fileinput='image']").change(function(){
		var parent = $(this).parents("div.image-with-preview");
	    readURLTrack(this);

	    function readURLTrack(input) {

	    if (input.files && input.files[0]) {
	        var reader = new FileReader();

		        reader.onload = function (e) {
		            parent.find('img.prev-img').attr('src', e.target.result).css("border-color","#5cb85c");
		        }

		        reader.readAsDataURL(input.files[0]);
		    }
		}
	});

	tinymce.init({
	    selector: 'textarea:not(.not-tinymce)',
			plugins: "lists,link,code",
	  });

	require('./jscolor');
}(jQuery);
