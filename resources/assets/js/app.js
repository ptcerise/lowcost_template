
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('./jquery.touchSwipe');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example', require('./components/Example.vue'));

const app = new Vue({
    el: '#app'
});

/*
	SLIDE DOWN BUTTON FUNCTION
*/
+ function($) {
    'use strict';
    $("#slideDown").click(function() {
        $('html, body').animate({
            scrollTop: $("#mainContent").offset().top - 100
        }, 1000);
    });
}(jQuery);

/*
	MAKE THE CONTENT BOX SAME HEIGHT AS THE SIBLINGS
*/
+function($){
	'use strict';
	var $wdwWidth = $(window).width();
	if($wdwWidth > 768){
		$(document).ready(function(){
			$(".section-content").each(function(){
        var $htext = $(this).find('.content-text').height();
        var $himg = $(this).find('.content-image').height();
        var $h = $(this).height();
        var $w = $(this).find('.right').width();
        if($htext > $himg){
          $(this).find('.content-image').css('height',$h+'px');
        }else{
          $(this).find('.content-text').css('height',$h+'px');
        }
				$(this).find('#map').css('height',$h+'px').css('width',$w+'px');
			});
		});
	}else{
		var $h = $('.content-box.right').height();
		var $w = $('.content-box.right').width();
		$('.content-box.right').find('#map').css('height',$h+'px').css('width',$w+'px');
	}
}(jQuery);

// ENTRANCE ANIMATION
+function($){
	'use strict';
    var $animation_elements = $('[data-animate="true"]');
    var $window = $(window);

    if($window.width() > 768){
      $window.on('scroll', check_if_in_view);
      $(document).ready(check_if_in_view);

      function check_if_in_view() {

        var window_height = $window.height();
        var window_top_position = $window.scrollTop();
        var window_bottom_position = (window_top_position + window_height);

        $.each($animation_elements, function() {
          var $element = $(this);
          var $effect = $(this).data('effect');
          var element_height = $element.outerHeight();
          var element_top_position = $element.offset().top;
          var element_bottom_position = (element_top_position + element_height);

          //check to see if this current container is within viewport
          if ((element_bottom_position >= window_top_position) &&
              (element_top_position <= window_bottom_position)) {
            $element.addClass('animated '+$effect);
          }
        });
      }
    }

}(jQuery);

+function($) {
    $(document).ready(function(){

        $(window).scroll(function(){
            if ($(this).scrollTop() > 125) {
                $('.navbar').addClass('scroll');
            } else {
                $('.navbar').removeClass('scroll');
            }
        });

    });
}(jQuery);

+function($){
	'use strict';
	var $wdwWidth = $(window).width();
	if($wdwWidth > 768){
		$(document).ready(function(){
			$("#cards").each(function(){
				var $h = $(this).height();
				$(this).find('.card').css('min-height',$h+'px');
			});
		});
	}else{
    $("#cards").each(function(){
      var $h = $(this).height();
      $(this).find('.card').css('min-height','auto');
    });
	}

	$(window).resize(function(){
		var $wdwWidth = $(window).width();
		if($wdwWidth > 768){
			$("#cards").each(function(){
				var $h = $(this).height();
				$(this).find('.card').css('min-height',$h+'px');
			});
		}else{
			$("#cards").each(function(){
				var $h = $(this).height();
				$(this).find('.card').css('min-height','auto');
			});
		}
	});
}(jQuery);

+function($){
  $(".carousel").swipe({

    swipe: function(event, direction, distance, duration, fingerCount, fingerData) {

      if (direction == 'left') $(this).carousel('next');
      if (direction == 'right') $(this).carousel('prev');

    },
    allowPageScroll:"vertical"

  });
}(jQuery);
