-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 07, 2017 at 02:21 PM
-- Server version: 10.0.31-MariaDB-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nimafenw_cincinemban`
--

-- --------------------------------------------------------

--
-- Table structure for table `general`
--

CREATE TABLE `general` (
  `id` int(10) UNSIGNED NOT NULL,
  `general_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `general_content` text COLLATE utf8mb4_unicode_ci,
  `general_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `general_page` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `general_section` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `general_desc` text COLLATE utf8mb4_unicode_ci,
  `temp_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `general`
--

INSERT INTO `general` (`id`, `general_title`, `general_content`, `general_url`, `general_page`, `general_section`, `general_desc`, `temp_id`, `created_at`, `updated_at`) VALUES
(1, '', 'my best slogan', 'http://localhost/section1', 'home', 'divider1', 'Divider Home #1', NULL, NULL, '2017-05-12 03:56:23'),
(2, '', 'Moto atau slogan perusahaan', 'http://localhost/about', 'home', 'divider2', 'Divider Home #2', NULL, NULL, '2017-05-08 21:37:19'),
(3, 'Judul Pembuka / Selamat datang', '<p style=\"text-align: center;\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n<p style=\"text-align: center;\"><a title=\"Contoh URL&nbsp;Usaha\" href=\"jogjabagus.com\" target=\"_blank\" rel=\"noopener noreferrer\">Contoh URL&nbsp;Usaha</a></p>', NULL, 'home', 'content', NULL, NULL, '2017-04-21 01:03:55', '2017-05-16 03:23:27');

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `id` int(10) UNSIGNED NOT NULL,
  `media_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `media_content` text COLLATE utf8mb4_unicode_ci,
  `media_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `media_page` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `media_section` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `media_desc` text COLLATE utf8mb4_unicode_ci,
  `temp_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`id`, `media_title`, `media_content`, `media_url`, `media_page`, `media_section`, `media_desc`, `temp_id`, `created_at`, `updated_at`) VALUES
(1, 'Judul Paragraf', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,</p>\r\n<p>sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>', 'banner1_20170509045525.jpg', 'home', 'banner-content1', 'Banner1', NULL, NULL, '2017-05-08 21:55:26'),
(2, 'Judul Paragraf', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,</p>\r\n<p>sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>', 'banner2_20170509045547.jpg', 'home', 'banner-content2', 'Banner2', NULL, NULL, '2017-05-08 21:55:48'),
(3, 'Page 1', '', 'banner_section1_20170518043339.jpg', 'section1', 'banner-header', 'Banner Header Section1', NULL, NULL, '2017-05-17 21:33:40'),
(4, 'Page 2', '', 'banner_section2_20170518043410.jpg', 'section2', 'banner-header', 'Banner Header Section2', NULL, NULL, '2017-07-13 02:43:15'),
(5, '', '', 'banner_20170519032233.jpg', 'about', 'banner-header', 'Banner Header About', NULL, NULL, '2017-05-18 20:22:33'),
(6, '', '', 'banner_20170518043732.jpg', 'contact', 'banner-header', 'Banner Header Contact', NULL, NULL, '2017-05-17 21:37:32'),
(10, 'Judul Konten 1', '<p style=\"text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>', 'content_20170518043633.jpg', 'about', 'content', 'About Content', NULL, '2017-05-08 21:52:48', '2017-05-17 21:36:33'),
(11, 'Judul Konten 2', '<p style=\"text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>', 'content_20170518043643.jpg', 'about', 'content', 'About Content', NULL, '2017-05-08 21:53:54', '2017-05-17 21:36:43'),
(12, 'Judul Konten 1', '<p style=\"text-align: left;\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>', 'content_section1_20170518043457.jpg', 'section1', 'content', 'Section1 Content', NULL, '2017-05-08 22:00:08', '2017-05-24 00:52:24'),
(13, 'Judul Konten 2', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>', 'content_section1_20170518043518.jpg', 'section1', 'content', 'Section1 Content', NULL, '2017-05-08 22:00:24', '2017-05-17 21:35:18'),
(14, 'Judul Konten 1', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>', 'content_section2_20170518043528.jpg', 'section2', 'content', 'Section2 Content', NULL, '2017-05-08 22:00:58', '2017-05-17 21:35:29'),
(15, 'Judul Konten 2', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>', 'content_section2_20170518043540.jpg', 'section2', 'content', 'Section2 Content', NULL, '2017-05-08 22:01:13', '2017-05-17 21:35:40'),
(18, 'Homestay', NULL, 'carousel_20170509084039.jpg', 'home', 'carousel', NULL, 2, '2017-05-09 01:40:42', '2017-05-16 03:23:49'),
(19, 'Cargo', NULL, 'carousel_20170509084216.jpg', 'home', 'carousel', NULL, 3, '2017-05-09 01:42:16', '2017-05-09 01:42:16'),
(20, 'Bus dan Travel', NULL, 'carousel_20170509084245.jpg', 'home', 'carousel', NULL, 4, '2017-05-09 01:42:46', '2017-05-09 01:42:46'),
(21, 'Judul Konten', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>', 'card_20170510070011.jpg', 'home', 'card', 'Kartu 1', 1, '2017-05-10 00:00:11', '2017-05-10 00:04:04'),
(22, 'Judul Konten', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>', 'card_20170510070030.jpg', 'home', 'card', 'Kartu 2', 2, '2017-05-10 00:00:30', '2017-05-10 00:04:23'),
(23, 'Judul Konten', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>', 'card_20170510070052.jpg', 'home', 'card', 'Kartu 3', 3, '2017-05-10 00:00:52', '2017-05-10 00:04:37');

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `id` int(10) UNSIGNED NOT NULL,
  `message_sender_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message_sender_email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message_sender_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `message_status` enum('1','0') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_03_23_071207_create_media_table', 1),
(4, '2017_03_23_071227_create_general_table', 1),
(5, '2017_03_23_071301_create_message_table', 1),
(6, '2017_03_23_071317_create_settings_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `setting_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `setting_value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `setting_desc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `setting_name`, `setting_value`, `setting_desc`, `created_at`, `updated_at`) VALUES
(1, 'companyLogo', 'logo20170519040529.png', 'Company Logo', NULL, '2017-05-18 21:05:29'),
(2, 'companyName', 'Nama Perusahaan', 'Company Name', NULL, '2017-05-18 20:35:19'),
(3, 'companyDesc', 'Website Perusaan', 'Company Description', NULL, '2017-05-08 21:49:35'),
(4, 'companyAddress', 'TSBC Slot B2 Jl. Tamansiswa No. 160 Mergangsan, Yogyakarta', 'Company Address', NULL, '2017-05-08 21:42:38'),
(5, 'companyLatLon', '-7.81857195223691,110.36468533192442', 'Company Latitude Longitude', NULL, '2017-05-10 04:03:55'),
(6, 'companyEmail', 'perusahaan.saya@mail.com', 'Company Email', NULL, '2017-05-08 21:43:03'),
(7, 'companyPhone', '0123-4567-8900', 'Company Phone', NULL, NULL),
(8, 'companyFacebook', 'http://facebook.com/perusahaan.saya', 'Company Facebook Account', NULL, '2017-05-08 21:43:25'),
(9, 'companyInstagram', 'http://instagram.com/perusahaan.saya', 'Company Instagram Account', NULL, '2017-05-08 21:43:25'),
(10, 'companyTwitter', 'http://twitter.com/perusahaan.saya', 'Company Twitter Account', NULL, '2017-05-08 21:43:25'),
(11, 'companyPinterest', NULL, 'Company Pinterest Account', NULL, '2017-08-02 20:19:41'),
(12, 'companyPath', NULL, 'Company Path Account', NULL, '2017-08-02 20:19:41'),
(13, 'companyGooglePlus', 'http://plus.google.com/perusahaan.saya', 'Company Google+ Account', NULL, '2017-05-08 21:43:25'),
(14, 'companyLinkedIn', 'http://linkedin.com/perusahaan.saya', 'Company LinkedIn Account', NULL, '2017-05-08 21:44:34'),
(15, 'companySnapchat', 'http://snapchat.com/perusahaan.saya', 'Company Snapchat Account', NULL, '2017-05-08 21:44:34'),
(16, 'pageName', 'Halaman A|Halaman B|Tentang Kami|Kontak', 'Page names', NULL, '2017-07-31 21:07:35'),
(17, 'companyFavicon', 'favicon20170519041702.ico', 'v=7', NULL, '2017-05-18 21:17:02'),
(18, 'webColor', '195898|FFFFFF|F4F4F4|000000', 'Website color scheme', NULL, '2017-08-02 20:32:31'),
(19, 'publishPage', 'publish|publish', 'Hide and Publish page', NULL, '2017-05-28 18:39:08'),
(20, 'btnColor', '195898|FFFFFF|0F355B|FFFFFF', 'Button color scheme', NULL, '2017-08-02 20:32:31'),
(21, 'txtBannerColor', 'FFFFFF', 'Text on banner color scheme', NULL, '2017-08-02 20:32:41'),
(22, 'navbarColor', 'EAEAEA|FFFFFF', 'Navbar / Divider / Card color scheme', NULL, '2017-08-02 20:32:57'),
(23, 'webFont', '|||||||', 'Website Font', '2017-08-02 23:22:14', '2017-08-02 23:22:14');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` enum('1','2') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '2',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `level`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', 'admin@web.com', '$2y$10$6B1bGnJMv70JkGnz6JeBfupvZ9zrFDVBvhjl7JjA3Z25ODbbIzSWi', '1', 'oCRVYEk7RhyNZnuLqzBeLVlHFa1owS6pkmKc2Q6CKgCzZ2suQkd51vcq68nT', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `general`
--
ALTER TABLE `general`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `general`
--
ALTER TABLE `general`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
