<?php

namespace App\Providers;

use App\Settings as mSetting;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $logo = mSetting::where('setting_name','companyLogo')->first()->setting_value;
        $favicon = mSetting::where('setting_name','companyFavicon')->first();
        $desc = mSetting::where('setting_name','companyDesc')->first()->setting_value;
        $name = mSetting::where('setting_name','companyName')->first()->setting_value;
        $address = mSetting::where('setting_name','companyAddress')->first()->setting_value;
        $phone = mSetting::where('setting_name','companyPhone')->first()->setting_value;
        $email = mSetting::where('setting_name','companyEmail')->first()->setting_value;
        $fb = mSetting::where('setting_name','companyFacebook')->first()->setting_value;
        $ig = mSetting::where('setting_name','companyInstagram')->first()->setting_value;
        $tw = mSetting::where('setting_name','companyTwitter')->first()->setting_value;
        $pt = mSetting::where('setting_name','companyPinterest')->first()->setting_value;
        $pa = mSetting::where('setting_name','companyPath')->first()->setting_value;
        $gp = mSetting::where('setting_name','companyGooglePlus')->first()->setting_value;
        $li = mSetting::where('setting_name','companyLinkedIn')->first()->setting_value;
        $sc = mSetting::where('setting_name','companySnapchat')->first()->setting_value;
        $page = explode('|',strtolower(mSetting::where(['setting_name'=>'pageName'])->first()->setting_value));
        $color = explode('|',strtoupper(mSetting::where(['setting_name'=>'webColor'])->first()->setting_value));
        $btncolor = explode('|',strtoupper(mSetting::where(['setting_name'=>'btnColor'])->first()->setting_value));
        $txtbannercolor = strtoupper(mSetting::where(['setting_name'=>'txtBannerColor'])->first()->setting_value);
        $navbarcolor = explode('|',strtoupper(mSetting::where(['setting_name'=>'navbarColor'])->first()->setting_value));
        $publish = explode('|',mSetting::where(['setting_name'=>'publishPage'])->first()->setting_value);
        $font = explode('|',mSetting::where(['setting_name'=>'webFont'])->first()->setting_value);

        $storage_url = config('customvar.storage_url');
        $asset_url = config('customvar.asset_url');
        $shareData = [
            'masterLogo'=>$logo,
            'masterFavicon'=>$favicon,
            'masterDesc'=>$desc,
            'masterName'=>$name,
            'masterAddress'=>$address,
            'masterPhone'=>$phone,
            'masterEmail'=>$email,
            'masterFb'=>$fb,
            'masterIg'=>$ig,
            'masterTw'=>$tw,
            'masterPt'=>$pt,
            'masterPa'=>$pa,
            'masterGp'=>$gp,
            'masterLi'=>$li,
            'masterSc'=>$sc,
            'storage_url'=>$storage_url,
            'asset_url'=>$asset_url,
            'pageName'=>$page,
            'webColor'=>$color,
            'btnColor'=>$btncolor,
            'txtBannerColor'=>$txtbannercolor,
            'navbarColor'=>$navbarcolor,
            'publishPage'=>$publish,
            'webFont'=>$font,
          ];

        View::share($shareData);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
