<?php

namespace App\Http\Controllers;

use App\Media as mMedia;
use App\General as mGeneral;
use App\Settings as mSetting;

use Illuminate\Http\Request;

class Sections extends Controller
{
    function index(Request $req){
    	$page = $req->segment(1);
      $pagedb = explode('|',str_replace(' ','-',mSetting::where(['setting_name'=>'pageName'])->first()->setting_value));

    	if($page == strtolower($pagedb[0])){
    		$section = 'section1';
    	}else{
    		$section = 'section2';
    	}

    	$data['section'] = $page;
      $data['page'] = $section;
    	$data['banner'] = mMedia::where(['media_page'=>$section,'media_section'=>'banner-header'])->first()->media_url;
      $data['caption'] = mMedia::where(['media_page'=>$section,'media_section'=>'banner-header'])->first()->media_title;
      $data['contents'] = mMedia::where(['media_page'=>$section,'media_section'=>'content'])->orderBy('temp_id','asc')->get();
    	return view('section')->with($data);
    }
}
