<?php

namespace App\Http\Controllers;

use App\Media as mMedia;
use App\General as mGeneral;
use App\Settings as mSetting;
use Illuminate\Http\Request;

class Home extends Controller
{
    function index(){
    	$data['carousel'] = mMedia::where(['media_page'=>'home','media_section'=>'carousel'])->orderBy('temp_id','asc')->get();
      $data['content'] = mGeneral::where(['general_page'=>'home','general_section'=>'content'])->first();
      $data['cards'] = mMedia::where(['media_page'=>'home','media_section'=>'card'])->orderBy('temp_id','asc')->get();
    	$data['divider1'] = mGeneral::where(['general_page'=>'home','general_section'=>'divider1'])->first();
    	$data['divider2'] = mGeneral::where(['general_page'=>'home','general_section'=>'divider2'])->first();
    	$data['banner1'] = mMedia::where(['media_page'=>'home','media_section'=>'banner-content1'])->first();
    	$data['banner2'] = mMedia::where(['media_page'=>'home','media_section'=>'banner-content2'])->first();
      $data['page'] = 'home';
    	return view('home')->with($data);
    }
}
