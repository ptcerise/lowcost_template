<?php

namespace App\Http\Controllers;

use Mapper;
use Validator;
use Mail;
use App\Mail\LowcostMail as LowcostMail;
use App\Settings as mSetting;
use App\Media as mMedia;
use App\Message as mMessage;
use Illuminate\Http\Request;

class Contact extends Controller
{
    function index(){
    	$location = explode(',',mSetting::where('setting_name','companyLatLon')->first()->setting_value);
	    $lat = $location[0];
	    $lng = $location[1];

    	Mapper::map($lat, $lng, ['markers' => ['title' => 'My Company']]);

    	$data['banner'] = mMedia::where(['media_page'=>'contact','media_section'=>'banner-header'])->first()->media_url;
    	$data['map'] = Mapper::render();
      $data['page'] = 'contact';
    	return view('contact')->with($data);
    }

    function sendMessage(Request $req){
    	$name = $req->name;
    	$email = $req->email;
    	$message = $req->message;

    	$this->validate($req, [
    	    'name' => 'required',
          'email' => 'email|required',
          'message'=>'required',
    	]);

  		$validator = Validator::make($req->all(), [
  		    'name' => 'required',
  	      'email' => 'email|required',
  	      'message'=>'required',
  		]);

  		if($validator->fails()){
  			$req->flash(['name', 'email','message']);
  		}

      $maildata = [
        'name'=>$name,
        'email'=>$email,
        'message'=>$message,
      ];

      $emailto = mSetting::where('setting_name','companyEmail')->first()->setting_value;
      Mail::to($emailto)->send(new LowcostMail($name, $email, $message));

      $db = new mMessage;
    	$db->message_sender_name = $name;
    	$db->message_sender_email = $email;
    	$db->message_sender_content = $message;
    	$db->save();

    	$req->session()->flash('message', 'Message sent successfully!');
		  $req->session()->flash('alert-class', 'alert-success');

    	return back();
    }
}
