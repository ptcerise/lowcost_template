<?php

namespace App\Http\Controllers;

use App\Media as mMedia;
use App\General as mGeneral;
use App\Settings as mSetting;

use Illuminate\Http\Request;

class About extends Controller
{
    function index(){
    	$data['banner'] = mMedia::where(['media_page'=>'about','media_section'=>'banner-header'])->first()->media_url;
    	$data['contents'] = mMedia::where(['media_page'=>'about','media_section'=>'content'])->orderBy('temp_id','asc')->get();
      $data['page'] = 'about';
    	return view('about')->with($data);
    }
}
