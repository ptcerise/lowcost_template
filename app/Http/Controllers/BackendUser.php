<?php

namespace App\Http\Controllers;

use Validator;

use App\UsersModel as mUser;
use Illuminate\Http\Request;

class BackendUser extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    function index(){
    	$data['users'] = mUser::all();
    	$data['page'] = 'users';
    	return view('backend.user')->with($data);
    }

    function register(Request $req){
    	$name = $req->name;
    	$email = $req->email;
    	$password = $req->password;
    	$password_confirm = $req->password_confirmation;

        $checkEmail = mUser::where(['email'=>$email])->first();

        if(count($checkEmail) != 0){
            $req->session()->flash('message', 'Email already exist!'); 
            $req->session()->flash('alert-class', 'alert-danger');
            return back();
        }

    	$this->validate($req, [
		    'name' => 'required',
	        'email' => 'email|required',
	        'password'=>'required',
	        'password_confirmation'=>'required|same:password',
		]);

		$db = new mUser;
    	$db->name = $name;
    	$db->email = $email;
    	$db->password = bcrypt($password);
    	$db->save();

    	$req->session()->flash('message', 'New user added successfully!'); 
		$req->session()->flash('alert-class', 'alert-success');

		return redirect()->route('backendUser');
    }

    function update($id){
    	$data['user'] = mUser::where(['id'=>$id])->first();

    	return view('auth.updateuser')->with($data);
    }

    function saveupdate(Request $req){
    	$id = $req->id;
    	$name = $req->name;
    	$email = $req->email;
    	$password = $req->password;
    	$password_confirm = $req->password_confirmation;

		if(empty($password)){
			$this->validate($req, [
				'name' => 'required',
		        'email' => 'required|email',
			]);
		}else{
			$this->validate($req, [
		        'email' => 'email',
		        'password_confirmation'=>'same:password',
			]);
		}

		$db = mUser::find($id);
    	$db->name = $name;
    	$db->email = $email;
    	if($password != ""){
    		$db->password = bcrypt($password);
    	}
    	$db->save();

    	$req->session()->flash('message', 'Updated successfully!'); 
		$req->session()->flash('alert-class', 'alert-success');

		return redirect()->route('backendUser');
    }

    function level(Request $req){
    	$id = $req->id;
    	$level = $req->level;
    	if($level == '1'){
    		$level = '2';
    	}else{
    		$level = '1';
    	}

    	$db = mUser::find($id);
    	$db->level = $level;
    	$db->save();

    	return back();
    }

    function delete(Request $req){
    	$id = $req->id;

    	mUser::find($id)->delete();

    	return back();
    }
}
