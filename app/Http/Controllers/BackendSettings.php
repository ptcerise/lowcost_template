<?php

namespace App\Http\Controllers;

use App\Settings as mSetting;
use App\Media as mMedia;
use Storage;
use Image;
use Carbon\Carbon;
use Illuminate\Http\Request;

class BackendSettings extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    function index(){
      $data['companyLogo'] = mSetting::where(['setting_name'=>'companyLogo'])->first();
			$data['companyFavicon'] = mSetting::where(['setting_name'=>'companyFavicon'])->first();
    	$data['companyName'] = mSetting::where(['setting_name'=>'companyName'])->first();
    	$data['companyDesc'] = mSetting::where(['setting_name'=>'companyDesc'])->first();

    	$data['companyName'] = mSetting::where(['setting_name'=>'companyName'])->first();
    	$data['companyDesc'] = mSetting::where(['setting_name'=>'companyDesc'])->first();
    	$data['companyEmail'] = mSetting::where(['setting_name'=>'companyEmail'])->first();
    	$data['companyPhone'] = mSetting::where(['setting_name'=>'companyPhone'])->first();
    	$data['companyAddress'] = mSetting::where(['setting_name'=>'companyAddress'])->first();
    	$data['page'] = "settings";
    	return view('backend.settings')->with($data);
    }

    function saveprofile(Request $req){
    	$nameId = $req->companyNameId;
    	$descId = $req->companyDescId;
			$logoId = $req->companyLogoId;
			$faviconId = $req->companyFaviconId;
			$faviconVersion = $req->faviconVersion;
    	$companyName = $req->companyName;
    	$companyDesc = $req->companyDesc;

    	$this->validate($req, [
				'companyLogo'=> 'image|mimes:png|max:1024|dimensions:min_width=200,min_height=200',
				'companyFavicon'=> 'mimes:ico|max:50|dimensions:min_width=32,min_height=32',
		    'companyName' => 'required',
	      'companyDesc' => 'required',
			]);

			$timestamp = str_replace([' ', ':', '-'], '', Carbon::now()->toDateTimeString());

			if($req->companyLogo):
				$x = explode('.',$req->companyLogo->getClientOriginalName());
				$ext = $x[1];
				$imageName = 'logo'.$timestamp.'.'.$ext;

				#### SAVE IMAGE
				$req->file('companyLogo')->storeAs(
						'img/', $imageName
				);

				#### RESIZE IMAGE
				Image::make(config('customvar.storage_url').'img/'.$imageName)->resize(200, 200)->save(config('customvar.storage_url').'img/'.$imageName);

				$filename = mSetting::where('setting_name','companyLogo')->first();
				$url = 'img/'.$filename->setting_value;
				Storage::delete($url);

				$db = mSetting::find($logoId);
				$db->setting_value = $imageName;
				$db->save();
			endif;
			if($req->companyFavicon):
				$x = explode('.',$req->companyFavicon->getClientOriginalName());
				$ext = $x[1];
				$imageName = 'favicon'.$timestamp.'.'.$ext;

				#### SAVE IMAGE
				$req->file('companyFavicon')->storeAs(
						'img/', $imageName
				);

				$filename = mSetting::where('setting_name','companyFavicon')->first();
				$url = 'img/'.$filename->setting_value;
				Storage::delete($url);

				$db = mSetting::find($faviconId);
				$db->setting_value = $imageName;
				$db->setting_desc = 'v='.($faviconVersion+1);
				$db->save();
			endif;

			$db = mSetting::find($nameId);
			$db->setting_value = $companyName;
			$db->save();

			$db = mSetting::find($descId);
			$db->setting_value = $companyDesc;
			$db->save();

			$req->session()->flash('message', 'Updated successfully!');
			$req->session()->flash('alert-class', 'alert-success');
	    $req->session()->flash('anchor', '#profile');

			return back();
    }

    function save_color(Request $req){
      ## MAIN COLOR
      $color = $req->color;
      $idcolor = $req->idcolor;

      $save = $color[0].'|'.$color[1].'|'.$color[2].'|'.$color[3];
      $db = mSetting::find($idcolor);
      $db->setting_value = $save;
      $db->save();

      ## BUTTON COLOR
      $color = $req->btn_color;
      $idcolor = $req->idbtncolor;

      $save = $color[0].'|'.$color[1].'|'.$color[2].'|'.$color[3];
      $db = mSetting::find($idcolor);
      $db->setting_value = $save;
      $db->save();

      ## TEXT ON BANNER COLOR
      $color = $req->text_onbanner;
      $idcolor = $req->idtxtbannercolor;

      $save = $color;
      $db = mSetting::find($idcolor);
      $db->setting_value = $save;
      $db->save();

      ## NAVBAR / DIVIDER / CARD COLOR
      $color = $req->panel_color;
      $idcolor = $req->idnavbarcolor;

      $save = $color[0].'|'.$color[1];
      $db = mSetting::find($idcolor);
      $db->setting_value = $save;
      $db->save();

    	$req->session()->flash('message-color', 'Updated successfully!');
		  $req->session()->flash('alert-class', 'alert-success');
      $req->session()->flash('anchor', '#message-color');

    	return back();
      // return $save;
    }

    function save_page_name(Request $req){
    	$pagename = $req->pagename;
      $id = $req->id;

      $save = $pagename[0].'|'.$pagename[1].'|'.$pagename[2].'|'.$pagename[3];
      $db = mSetting::find($id);
      $db->setting_value = $save;
      $db->save();

    	$req->session()->flash('message-pagename', 'Updated successfully!');
		  $req->session()->flash('alert-class', 'alert-success');
      $req->session()->flash('anchor', '#message-pagename');

    	return back();
      // return $save;
    }

    function page_publish(Request $req){
      $page = $req->page;
      $stat = $req->stat;
      $id = mSetting::where(['setting_name'=>'publishPage'])->first()->id;
      $content = explode('|',mSetting::where(['setting_name'=>'publishPage'])->first()->setting_value);

      $db = mSetting::find($id);
      if($page == "section1"):
        $db->setting_value = $stat.'|'.$content[1];
      else:
        $db->setting_value = $content[0].'|'.$stat;
      endif;
      $db->save();

      $data = [
        'page'=>$page,
        'stat'=>$stat
      ];

      return json_encode($data);
    }

    function savefont(Request $req){
      $fonts = $req->fontselect;
      $id = mSetting::where(['setting_name'=>'webFont'])->first()->id;

      $save = $fonts[0].'|'.$fonts[1].'|'.$fonts[2].'|'.$fonts[3].'|'.$fonts[4].'|'.$fonts[5].'|'.$fonts[6].'|'.$fonts[7];
      $db = mSetting::find($id);
      $db->setting_value = $save;
      $db->save();

      $req->session()->flash('message-fontselect', 'Updated successfully!');
		  $req->session()->flash('alert-class', 'alert-success');
      $req->session()->flash('anchor', '#message-fontselect');

    	return back();
    }
}
