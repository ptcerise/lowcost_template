<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BackendDashboard extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    
    function index(Request $req){
    	$data['page'] = 'dashboard';
    	return view('backend.dashboard')->with($data);
    }
}
