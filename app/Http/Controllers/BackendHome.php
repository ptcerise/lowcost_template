<?php

namespace App\Http\Controllers;

use App\Media as mMedia;
use App\General as mGeneral;
use Storage;
use Image;
use Carbon\Carbon;
use Illuminate\Http\Request;

class BackendHome extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    function index(Request $req){
    	$data['carousel'] = mMedia::where(['media_page'=>'home','media_section'=>'carousel'])->orderBy('temp_id','asc')->get();
      $data['cards'] = mMedia::where(['media_page'=>'home', 'media_section'=>'card'])->orderBy('temp_id','asc')->get();
      $data['content'] = mGeneral::where(['general_page'=>'home','general_section'=>'content'])->first();
    	$data['divider1'] = mGeneral::where(['general_page'=>'home','general_section'=>'divider1'])->first();
    	$data['divider2'] = mGeneral::where(['general_page'=>'home','general_section'=>'divider2'])->first();
    	$data['banner1'] = mMedia::where(['media_page'=>'home','media_section'=>'banner-content1'])->first();
    	$data['banner2'] = mMedia::where(['media_page'=>'home','media_section'=>'banner-content2'])->first();

    	$data['page'] = 'home';
    	return view('backend.home')->with($data);
    }

    function save_carousel(Request $req){
      $id = $req->carousel_id;
      $title = $req->imagetitle;
      $alt = $req->imagealt;

      if(empty($id)): // THIS WOULD 'INSERT' NEW RECORD IF ID IS EMPTY
      	$this->validate($req, [
            'imagefile' => 'required|image|mimes:jpeg,jpg|max:1024|dimensions:min_width=1600,min_height=575',
        ]);

        $timestamp = str_replace([' ', ':', '-'], '', Carbon::now()->toDateTimeString());
        $x = explode('.',$req->imagefile->getClientOriginalName());
        $ext = $x[1];
        $imageName = 'carousel_'.$timestamp.'.'.$ext;

        #### SAVE IMAGE
        $req->file('imagefile')->storeAs(
            'img/home', $imageName
        );

        #### RESIZE IMAGE
        Image::make(config('customvar.storage_url').'img/home/'.$imageName)->resize(1600, 575)->save(config('customvar.storage_url').'img/home/'.$imageName);

        #### COMPRESS IMAGE FILE SIZE ####
        function compress_image($source_url, $destination_url, $quality) {
    			$info = getimagesize($source_url);

    			if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source_url);
    			elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($source_url);
    			elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($source_url);

    			//save file
    			imagejpeg($image, $destination_url, $quality);

    			//return destination file
    			return $destination_url;
    		}

    		$src = config('customvar.storage_url').'img/home'.'/'.$imageName;
    		$dest = $src;
    		$compressed = compress_image($src, $dest, 95);
    		#### END OF COMPRESS IMAGE ####

        $db = new mMedia;
      	$db->media_url = $imageName;
        $db->media_title = $title;
        $db->media_content = $alt;
      	$db->media_page = "home";
      	$db->media_section = "carousel";
        $last = mMedia::where(['media_page'=>'home','media_section'=>'carousel'])->orderBy('created_at','desc')->first();
      	if($last){
          $db->temp_id = $last->temp_id+1;
        }else{
          $db->temp_id = 1;
        }
      	$db->save();
      else:
        if($req->imagefile): // SAVE IMAGE IF THERE'S IMAGE UPLOADED
          $this->validate($req, [
              'imagefile' => 'required|image|mimes:jpeg,jpg|max:1024|dimensions:min_width=1600,min_height=575',
          ]);

          $timestamp = str_replace([' ', ':', '-'], '', Carbon::now()->toDateTimeString());
          $x = explode('.',$req->imagefile->getClientOriginalName());
          $ext = $x[1];
          $imageName = 'carousel_'.$timestamp.'.'.$ext;

          #### SAVE IMAGE
          $req->file('imagefile')->storeAs(
              'img/home', $imageName
          );

          #### RESIZE IMAGE
          Image::make(config('customvar.storage_url').'img/home/'.$imageName)->resize(1600, 575)->save(config('customvar.storage_url').'img/home/'.$imageName);

          #### COMPRESS IMAGE FILE SIZE ####
          function compress_image($source_url, $destination_url, $quality) {
      			$info = getimagesize($source_url);

      			if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source_url);
      			elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($source_url);
      			elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($source_url);

      			//save file
      			imagejpeg($image, $destination_url, $quality);

      			//return destination file
      			return $destination_url;
      		}

      		$src = config('customvar.storage_url').'img/home'.'/'.$imageName;
      		$dest = $src;
      		$compressed = compress_image($src, $dest, 95);
      		#### END OF COMPRESS IMAGE ####

          $filename = mMedia::where('id',$id)->first();
          $url = '/img/home/'.$filename->media_url;
          Storage::delete($url);

          $db = mMedia::find($id);
        	$db->media_url = $imageName;
          $db->media_title = $title;
          $db->media_content = $alt;
        	$db->save();
        else:
          $db = mMedia::find($id);
          $db->media_title = $title;
          $db->media_content = $alt;
        	$db->save();
        endif;
      endif;

    	$req->session()->flash('message-carousel', 'Uploaded successfully!');
  		$req->session()->flash('alert-class', 'alert-success');
  		$req->session()->flash('anchor', '#message-carousel');

  		return back();
    }

    function order_carousel(Request $req){
      $order = $req->reorder;

      foreach($order as $key => $val){
        $db = mMedia::find($val);
        $db->temp_id = $key+1;
        $db->save();
      }

      $req->session()->flash('message-carousel', 'Carousel rearranged!');
  		$req->session()->flash('alert-class', 'alert-success');
  		$req->session()->flash('anchor', '#message-carousel');

  		return back();
    }

    function delete_carousel(Request $req){
        $id = $req->id;

        $filename = mMedia::where('id',$id)->first();
        $url = '/img/home/'.$filename->media_url;
        Storage::delete($url);

        mMedia::find($id)->delete();

        return back();
    }

    function save_content(Request $req){
      $id = $req->id;
      $title = $req->title;
      $text = $req->text;

      $this->validate($req, [
          'title' => 'required',
          'text' => 'required',
      ]);

      if(empty($id)): // THIS WOULD BE INSERT
        $db = new mGeneral;
        $db->general_title = $title;
        $db->general_content = $text;
        $db->general_page = "home";
        $db->general_section = "content";
        $db->save();
      else: // THIS WOULD BE UPDATE
        $db = mGeneral::find($id);
        $db->general_title = $title;
        $db->general_content = $text;
        $db->save();
      endif;

      $req->session()->flash('message-home-content', 'Updated successfully!');
  		$req->session()->flash('alert-class', 'alert-success');
  		$req->session()->flash('anchor', '#message-home-content');

  		return back();
    }

    function save_divider(Request $req){
    	$id1 = $req->id1;
    	$id2 = $req->id2;
    	$divider1 = $req->divider1;
    	$divider2 = $req->divider2;

    	$this->validate($req, [
            'divider1' => 'required',
            'divider2' => 'required',
        ]);

        $save = [
        	$id1 => $divider1,
        	$id2 => $divider2
        ];

        foreach ($save as $key => $val) {
        	$db = mGeneral::find($key);
	        $db->general_content = $val;
	        $db->save();
        }

    	$req->session()->flash('message-divider', 'Updated successfully!');
  		$req->session()->flash('alert-class', 'alert-success');
  		$req->session()->flash('anchor', '#message-divider');

  		return back();
    }

    function save_banner(Request $req){
    	$type = $req->bannertype;
    	$id = $req->bannerid;

    	if($type == 'banner1'){
    		### BANNER 1 ###

    		$this->validate($req, [
	            'image_banner1' => 'image|mimes:jpeg,jpg|max:1024|dimensions:min_width=1600,min_height=550',
	            'title_banner1' => 'required',
	            'content_banner1' => 'required',
	        ]);

			if($req->image_banner1){
				$timestamp = str_replace([' ', ':', '-'], '', Carbon::now()->toDateTimeString());
				$x = explode('.',$req->image_banner1->getClientOriginalName());
				$ext = $x[1];
				$imageName = 'banner1_'.$timestamp.'.'.$ext;

				#### SAVE IMAGE
				$req->file('image_banner1')->storeAs(
					'img/home', $imageName
				);

				#### RESIZE IMAGE
				Image::make(config('customvar.storage_url').'img/home/'.$imageName)->resize(1600, 550)->save(config('customvar.storage_url').'img/home/'.$imageName);

				#### COMPRESS IMAGE FILE SIZE ####
				function compress_image($source_url, $destination_url, $quality) {
					$info = getimagesize($source_url);

					if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source_url);
					elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($source_url);
					elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($source_url);

					//save file
					imagejpeg($image, $destination_url, $quality);

					//return destination file
					return $destination_url;
				}

				$src = config('customvar.storage_url').'img/home'.'/'.$imageName;
				$dest = $src;
				$compressed = compress_image($src, $dest, 95);
				#### END OF COMPRESS IMAGE ####

				$filename = mMedia::where('id',$id)->first();
		        $url = '/img/home/'.$filename->media_url;
		        Storage::delete($url);

				$media_url = $imageName;
			}

			$title = $req->title_banner1;
    		$content = $req->content_banner1;


    	}else{
    		### BANNER 2 ###

    		$this->validate($req, [
	            'image_banner2' => 'image|mimes:jpeg,jpg|max:1024|dimensions:min_width=1600,min_height=550',
	            'title_banner2' => 'required',
	            'content_banner2' => 'required',
	        ]);

			if($req->image_banner2){
								$timestamp = str_replace([' ', ':', '-'], '', Carbon::now()->toDateTimeString());
				$x = explode('.',$req->image_banner2->getClientOriginalName());
				$ext = $x[1];
				$imageName = 'banner2_'.$timestamp.'.'.$ext;

				#### SAVE IMAGE
				$req->file('image_banner2')->storeAs(
					'img/home', $imageName
				);

				#### RESIZE IMAGE
				Image::make(config('customvar.storage_url').'img/home/'.$imageName)->resize(1600, 550)->save(config('customvar.storage_url').'img/home/'.$imageName);

				#### COMPRESS IMAGE FILE SIZE ####
				function compress_image($source_url, $destination_url, $quality) {
					$info = getimagesize($source_url);

					if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source_url);
					elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($source_url);
					elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($source_url);

					//save file
					imagejpeg($image, $destination_url, $quality);

					//return destination file
					return $destination_url;
				}

				$src = config('customvar.storage_url').'img/home'.'/'.$imageName;
				$dest = $src;
				$compressed = compress_image($src, $dest, 95);
				#### END OF COMPRESS IMAGE ####

				$filename = mMedia::where('id',$id)->first();
		        $url = '/img/home/'.$filename->media_url;
		        Storage::delete($url);

				$media_url = $imageName;
			}

			$title = $req->title_banner2;
    		$content = $req->content_banner2;

    	}

    	$db = mMedia::find($id);
    	$db->media_title = $title;
    	$db->media_content = $content;
  		if(!empty($media_url)){
  			$db->media_url = $media_url;
  		}
      	$db->media_desc = ucfirst($type);
      	$db->save();

      	$req->session()->flash('message-'.$type, 'Updated successfully!');
  		$req->session()->flash('alert-class', 'alert-success');
  		$req->session()->flash('anchor', '#message-'.$type);

      	return back();
    }

    function save_card(Request $req){
      $id = $req->card_id;
      $title = $req->cardtitle;
      $content = $req->cardcontent;
      $alt = $req->imagealt;

      if(empty($id)): // THIS WOULD 'INSERT' NEW RECORD IF ID IS EMPTY
      	$this->validate($req, [
            'imagefilecard' => 'required|image|mimes:jpeg,jpg|max:1024|dimensions:min_width=300,min_height=300',
        ]);

        $timestamp = str_replace([' ', ':', '-'], '', Carbon::now()->toDateTimeString());
        $x = explode('.',$req->imagefilecard->getClientOriginalName());
        $ext = $x[1];
        $imageName = 'card_'.$timestamp.'.'.$ext;

        #### SAVE IMAGE
        $req->file('imagefilecard')->storeAs(
            'img/home', $imageName
        );

        #### RESIZE IMAGE
        Image::make(config('customvar.storage_url').'img/home/'.$imageName)->resize(300, 300)->save(config('customvar.storage_url').'img/home/'.$imageName);

        #### COMPRESS IMAGE FILE SIZE ####
        function compress_image($source_url, $destination_url, $quality) {
    			$info = getimagesize($source_url);

    			if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source_url);
    			elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($source_url);
    			elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($source_url);

    			//save file
    			imagejpeg($image, $destination_url, $quality);

    			//return destination file
    			return $destination_url;
    		}

    		$src = config('customvar.storage_url').'img/home'.'/'.$imageName;
    		$dest = $src;
    		$compressed = compress_image($src, $dest, 95);
    		#### END OF COMPRESS IMAGE ####

        $db = new mMedia;
      	$db->media_url = $imageName;
        $db->media_title = $title;
        $db->media_content = $content;
      	$db->media_page = "home";
      	$db->media_section = "card";
        $db->media_desc = $alt;
        $last = mMedia::where(['media_page'=>'home','media_section'=>'card'])->orderBy('created_at','desc')->first();
      	if($last){
          $db->temp_id = $last->temp_id+1;
        }else{
          $db->temp_id = 1;
        }
      	$db->save();
      else:
        if($req->imagefilecard): // SAVE IMAGE IF THERE'S IMAGE UPLOADED
          $this->validate($req, [
              'imagefilecard' => 'required|image|mimes:jpeg,jpg|max:1024|dimensions:min_width=300,min_height=300',
          ]);

          $timestamp = str_replace([' ', ':', '-'], '', Carbon::now()->toDateTimeString());
          $x = explode('.',$req->imagefilecard->getClientOriginalName());
          $ext = $x[1];
          $imageName = 'card_'.$timestamp.'.'.$ext;

          #### SAVE IMAGE
          $req->file('imagefilecard')->storeAs(
              'img/home', $imageName
          );

          #### RESIZE IMAGE
          Image::make(config('customvar.storage_url').'img/home/'.$imageName)->resize(300, 300)->save(config('customvar.storage_url').'img/home/'.$imageName);

          #### COMPRESS IMAGE FILE SIZE ####
          function compress_image($source_url, $destination_url, $quality) {
      			$info = getimagesize($source_url);

      			if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source_url);
      			elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($source_url);
      			elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($source_url);

      			//save file
      			imagejpeg($image, $destination_url, $quality);

      			//return destination file
      			return $destination_url;
      		}

      		$src = config('customvar.storage_url').'img/home'.'/'.$imageName;
      		$dest = $src;
      		$compressed = compress_image($src, $dest, 95);
      		#### END OF COMPRESS IMAGE ####

          $filename = mMedia::where('id',$id)->first();
          $url = '/img/home/'.$filename->media_url;
          Storage::delete($url);

          $db = mMedia::find($id);
        	$db->media_url = $imageName;
          $db->media_title = $title;
          $db->media_content = $content;
          $db->media_desc = $alt;
        	$db->save();
        else:
          $db = mMedia::find($id);
          $db->media_title = $title;
          $db->media_content = $content;
          $db->media_desc = $alt;
        	$db->save();
        endif;
      endif;

    	$req->session()->flash('message-card', 'Uploaded successfully!');
  		$req->session()->flash('alert-class', 'alert-success');
  		$req->session()->flash('anchor', '#message-card');

  		return back();
    }

    function order_card(Request $req){
      $order = $req->reorder;

      foreach($order as $key => $val){
        $db = mMedia::find($val);
        $db->temp_id = $key+1;
        $db->save();
      }

      $req->session()->flash('message-card', 'Cards rearranged!');
  		$req->session()->flash('alert-class', 'alert-success');
  		$req->session()->flash('anchor', '#message-card');

  		return back();
    }

    function delete_card(Request $req){
        $id = $req->id;

        $filename = mMedia::where('id',$id)->first();
        $url = '/img/home/'.$filename->media_url;
        Storage::delete($url);

        mMedia::find($id)->delete();

        return back();
    }
}
