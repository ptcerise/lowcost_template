<?php

namespace App\Http\Controllers;

use App\Settings as mSetting;
use App\Media as mMedia;
use App\Message as mMessage;
use Storage;
use Image;
use Carbon\Carbon;
use Illuminate\Http\Request;

class BackendContact extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    function index(){
    	$data['companyEmail'] = mSetting::where(['setting_name'=>'companyEmail'])->first();
    	$data['companyPhone'] = mSetting::where(['setting_name'=>'companyPhone'])->first();
    	$data['companyAddress'] = mSetting::where(['setting_name'=>'companyAddress'])->first();
      $data['companyLatLon'] = mSetting::where(['setting_name'=>'companyLatLon'])->first();

    	$data['companyFacebook'] = mSetting::where(['setting_name'=>'companyFacebook'])->first();
    	$data['companyInstagram'] = mSetting::where(['setting_name'=>'companyInstagram'])->first();
    	$data['companyTwitter'] = mSetting::where(['setting_name'=>'companyTwitter'])->first();
      $data['companyPinterest'] = mSetting::where('setting_name','companyPinterest')->first();
      $data['companyPath'] = mSetting::where('setting_name','companyPath')->first();
      $data['companyGplus'] = mSetting::where('setting_name','companyGooglePlus')->first();
      $data['companyLinkedIn'] = mSetting::where('setting_name','companyLinkedIn')->first();
      $data['companySnapchat'] = mSetting::where('setting_name','companySnapchat')->first();

      $data['banner'] = mMedia::where(['media_page'=>'contact','media_section'=>'banner-header'])->first();
      $data['messages'] = mMessage::all();

    	$data['page'] = "contact";
    	return view('backend.contact')->with($data);
    }

    function savecontactinfo(Request $req){
    	$emailId = $req->companyEmailId;
    	$phoneId = $req->companyPhoneId;
    	$addressId = $req->companyAddressId;
      $latlonId = $req->companyLatLonId;
    	$companyEmail = $req->companyEmail;
    	$companyPhone = $req->companyPhone;
    	$companyAddress = $req->companyAddress;
      $companyLatLon = $req->companyLat.','.$req->companyLon;

    	$this->validate($req, [
		    'companyEmail' => 'email|required',
	      'companyPhone' => 'required',
	      'companyAddress' => 'required',
		  ]);

		$save = [$emailId=>$companyEmail, $phoneId=>$companyPhone, $addressId=>$companyAddress, $latlonId=>$companyLatLon];

		foreach ($save as $key => $val) {
			$db = mSetting::find($key);
			$db->setting_value = $val;
			$db->save();
		}

		$req->session()->flash('message-contact-info', 'Updated successfully!');
		$req->session()->flash('alert-class', 'alert-success');
        $req->session()->flash('anchor', '#contact-info');

		return back();
    }

    function savesocmed(Request $req){
    	$fbId = $req->companyFacebookId;
    	$igId = $req->companyInstagramId;
    	$twId = $req->companyTwitterId;
      $ptId = $req->companyPinterestId;
    	$paId = $req->companyPathId;
    	$gpId = $req->companyGplusId;
      $liId = $req->companyLinkedInId;
      $scId = $req->companySnapchatId;

    	$companyFacebook = $req->companyFacebook;
    	$companyInstagram = $req->companyInstagram;
    	$companyTwitter = $req->companyTwitter;
      $companyPinterest = $req->companyPinterest;
      $companyPath = $req->companyPath;
      $companyGplus = $req->companyGplus;
      $companyLinkedIn = $req->companyLinkedIn;
      $companySnapchat = $req->companySnapchat;

    	$this->validate($req, [
		    'companyFacebook' => 'nullable|url',
	      'companyInstagram' => 'nullable|url',
	      'companyTwitter' => 'nullable|url',
        'companyPinterest' => 'nullable|url',
	      'companyPath' => 'nullable|url',
	      'companyGplus' => 'nullable|url',
        'companyLinkedIn' => 'nullable|url',
        'companySnapchat' => 'nullable|url',
		]);

		$save = [
      $fbId=>$companyFacebook,
      $igId=>$companyInstagram,
      $twId=>$companyTwitter,
      $ptId=>$companyPinterest,
      $paId=>$companyPath,
      $gpId=>$companyGplus,
      $liId=>$companyLinkedIn,
      $scId=>$companySnapchat,
    ];

		foreach ($save as $key => $val) {
			$db = mSetting::find($key);
			$db->setting_value = $val;
			$db->save();
		}

		$req->session()->flash('message-socmed', 'Updated successfully!');
		$req->session()->flash('alert-class', 'alert-success');
    $req->session()->flash('anchor', '#socmed');

		return back();
    }

    function save_banner(Request $req){
        $id = $req->id;

        $this->validate($req, [
            'banner_section' => 'required|image|mimes:jpeg,jpg|max:1024|dimensions:min_width=1600,min_height=320',
        ]);

        $timestamp = str_replace([' ', ':', '-'], '', Carbon::now()->toDateTimeString());
        $x = explode('.',$req->banner_section->getClientOriginalName());
        $ext = $x[1];
        $imageName = 'banner_'.$timestamp.'.'.$ext;

        #### SAVE IMAGE
        $req->file('banner_section')->storeAs(
            'img/contact', $imageName
        );

        #### RESIZE IMAGE
        Image::make(config('customvar.storage_url').'img/contact/'.$imageName)->resize(1600, 320)->save(config('customvar.storage_url').'img/contact/'.$imageName);

        #### COMPRESS IMAGE FILE SIZE ####
        function compress_image($source_url, $destination_url, $quality) {
            $info = getimagesize($source_url);

            if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source_url);
            elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($source_url);
            elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($source_url);

            //save file
            imagejpeg($image, $destination_url, $quality);

            //return destination file
            return $destination_url;
        }

        $src = config('customvar.storage_url').'img/contact'.'/'.$imageName;
        $dest = $src;
        $compressed = compress_image($src, $dest, 95);
        #### END OF COMPRESS IMAGE ####

        $filename = mMedia::where('id',$id)->first();
        $url = '/img/contact/'.$filename->media_url;
        Storage::delete($url);

        $db = mMedia::find($id);
        $db->media_url = $imageName;
        $db->save();

        $req->session()->flash('message-banner', 'Uploaded successfully!');
        $req->session()->flash('alert-class', 'alert-success');
        $req->session()->flash('anchor', '#message-banner');

        return back();
    }

    function read_message(Request $req){
        $id = $req->id;

        $db = mMessage::find($id);
        $db->message_status = "0";
        $db->save();
    }

    function delete_message(Request $req){
        $id = $req->id;
        mMessage::find($id)->delete();
        return back();
    }
}
