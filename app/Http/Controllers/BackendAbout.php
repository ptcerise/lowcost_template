<?php

namespace App\Http\Controllers;

use App\Settings as mSetting;
use App\Media as mMedia;
use Storage;
use Image;
use Carbon\Carbon;
use Illuminate\Http\Request;

class BackendAbout extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    function index(){
      $data['banner'] = mMedia::where(['media_page'=>'about','media_section'=>'banner-header'])->first();
      $data['contents'] = mMedia::where(['media_page'=>'about','media_section'=>'content'])->orderBy('temp_id','asc')->get();

    	$data['page'] = "about";
    	return view('backend.about')->with($data);
    }

    function save_banner(Request $req){
        $id = $req->id;

        $this->validate($req, [
            'banner_section' => 'required|image|mimes:jpeg,jpg,png|max:1024|dimensions:min_width=1600,min_height=320',
        ]);

        $timestamp = str_replace([' ', ':', '-'], '', Carbon::now()->toDateTimeString());
        $x = explode('.',$req->banner_section->getClientOriginalName());
        $ext = $x[1];
        $imageName = 'banner_'.$timestamp.'.'.$ext;

        #### SAVE IMAGE
        $req->file('banner_section')->storeAs(
            'img/about', $imageName
        );

        #### RESIZE IMAGE
        Image::make(config('customvar.storage_url').'img/about/'.$imageName)->resize(1600, 320)->save(config('customvar.storage_url').'img/about/'.$imageName);

        #### COMPRESS IMAGE FILE SIZE ####
        function compress_image($source_url, $destination_url, $quality) {
            $info = getimagesize($source_url);

            if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source_url);
            elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($source_url);
            elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($source_url);

            //save file
            imagejpeg($image, $destination_url, $quality);

            //return destination file
            return $destination_url;
        }

        $src = config('customvar.storage_url').'img/about'.'/'.$imageName;
        $dest = $src;
        $compressed = compress_image($src, $dest, 95);
        #### END OF COMPRESS IMAGE ####

        $filename = mMedia::where('id',$id)->first();
        $url = '/img/about/'.$filename->media_url;
        Storage::delete($url);

        $db = mMedia::find($id);
        $db->media_url = $imageName;
        $db->save();

        $req->session()->flash('message-banner', 'Uploaded successfully!');
        $req->session()->flash('alert-class', 'alert-success');
        $req->session()->flash('anchor', '#message-banner');

        return back();
    }

    function save_content(Request $req){
        $id = $req->id;
        $title = $req->title_content;
        $content = $req->text_content;

        if(!empty($id)){
            ################### UPDATE #####################
            $this->validate($req, [
                'text_content'=>'required',
            ]);

            if($req->image_content){

                $this->validate($req, [
                    'image_content' => 'image|mimes:jpeg,jpg,png|max:1024|dimensions:min_width=800,min_height=600',
                ]);
                $timestamp = str_replace([' ', ':', '-'], '', Carbon::now()->toDateTimeString());
                $x = explode('.',$req->image_content->getClientOriginalName());
                $ext = $x[1];
                $imageName = 'content_'.$timestamp.'.'.$ext;

                #### SAVE IMAGE
                $req->file('image_content')->storeAs(
                    'img/about', $imageName
                );

                #### RESIZE IMAGE
                Image::make(config('customvar.storage_url').'img/about/'.$imageName)->resize(800, 600)->save(config('customvar.storage_url').'img/about/'.$imageName);

                #### COMPRESS IMAGE FILE SIZE ####
                function compress_image($source_url, $destination_url, $quality) {
                    $info = getimagesize($source_url);

                    if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source_url);
                    elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($source_url);
                    elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($source_url);

                    //save file
                    imagejpeg($image, $destination_url, $quality);

                    //return destination file
                    return $destination_url;
                }

                $src = config('customvar.storage_url').'img/about'.'/'.$imageName;
                $dest = $src;
                $compressed = compress_image($src, $dest, 95);
                #### END OF COMPRESS IMAGE ####

                $filename = mMedia::where('id',$id)->first();
                $url = '/img/about/'.$filename->media_url;
                Storage::delete($url);

                $db = mMedia::find($id);
                $db->media_url = $imageName;
                $db->media_title = $title;
                $db->media_content = $content;
                $db->save();
            }else{
                $db = mMedia::find($id);
                $db->media_title = $title;
                $db->media_content = $content;
                $db->save();
            }
        }else{
            ############## INSERT ################
            $this->validate($req, [
                'image_content' => 'required|image|mimes:jpeg,jpg,png|max:1024|dimensions:min_width=800,min_height=600',
                'text_content'=>'required',
            ]);

            $timestamp = str_replace([' ', ':', '-'], '', Carbon::now()->toDateTimeString());
            $x = explode('.',$req->image_content->getClientOriginalName());
            $ext = $x[1];
            $imageName = 'content_'.$timestamp.'.'.$ext;

            #### SAVE IMAGE
            $req->file('image_content')->storeAs(
                'img/about', $imageName
            );

            #### RESIZE IMAGE
            Image::make(config('customvar.storage_url').'img/about/'.$imageName)->resize(800, 600)->save(config('customvar.storage_url').'img/about/'.$imageName);

            #### COMPRESS IMAGE FILE SIZE ####
            function compress_image($source_url, $destination_url, $quality) {
                $info = getimagesize($source_url);

                if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source_url);
                elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($source_url);
                elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($source_url);

                //save file
                imagejpeg($image, $destination_url, $quality);

                //return destination file
                return $destination_url;
            }

            $src = config('customvar.storage_url').'img/about'.'/'.$imageName;
            $dest = $src;
            $compressed = compress_image($src, $dest, 95);
            #### END OF COMPRESS IMAGE ####

            $db = new mMedia;
            $db->media_url = $imageName;
            $db->media_title = $title;
            $db->media_content = $content;
            $db->media_page = 'about';
            $db->media_section = "content";
            $db->media_desc = "About Content";
            $db->save();
        }

        if(!empty($id)){
            $req->session()->flash('message-content', 'Updated successfully!');
        }else{
            $req->session()->flash('message-content', 'Added successfully!');
        }

        $req->session()->flash('alert-class', 'alert-success');
        $req->session()->flash('anchor', '#message-content');

        return back();
    }

    function delete_content(Request $req){
        $id = $req->id;

        $filename = mMedia::where('id',$id)->first();
        $url = '/img/about/'.$filename->media_url;
        Storage::delete($url);

        mMedia::find($id)->delete();

        return back();
    }

		function order_content(Request $req){
      $order = $req->reorder;

      foreach($order as $key => $val){
        $db = mMedia::find($val);
        $db->temp_id = $key+1;
        $db->save();
      }

      $req->session()->flash('message-content', 'Content rearranged!');
  		$req->session()->flash('alert-class', 'alert-success');
  		$req->session()->flash('anchor', '#message-content');

  		return back();
    }
}
