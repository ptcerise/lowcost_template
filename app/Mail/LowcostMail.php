<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class LowcostMail extends Mailable
{
    use Queueable, SerializesModels;
    
    public $name;
    public $email;
    public $content;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $email, $content)
    {
        $this->name = $name;
        $this->email = $email;
        $this->content = $content;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $name = $this->name;
        $email = $this->email;
        $message = $this->content;
        return $this->view('email.text-email')
                    ->from($email, $name)
                    ->replyTo($email, $name)
                    ->subject('Website Message '.date('d/m/Y'));
    }
}
