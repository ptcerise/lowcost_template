<?php

return [

    /*
    |--------------------------------------------------------------------------
    | User Defined Variables
    |--------------------------------------------------------------------------
    |
    | This is a set of variables that are made specific to this application
    | that are better placed here rather than in .env file.
    | Use config('your_key') to get the values.
    |
    */

    /* FOR LOCALHOST */
    'storage_url' => 'storage/',
    'asset_url' => '',

    /* FOR HOSTING
    'storage_url' => 'storage/app/public/',
     'asset_url' => 'public/',
    */

];