<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert(array(
        	array(
            	'setting_name' => 'companyLogo',
            	'setting_value' => 'logo.png',
            	'setting_desc' => 'Company Logo',
        	),
          array(
            	'setting_name' => 'companyFavicon',
            	'setting_value' => 'favicon.ico',
            	'setting_desc' => 'Company Favicon',
        	),
        	array(
            	'setting_name' => 'companyName',
            	'setting_value' => 'My Company',
            	'setting_desc' => 'Company Name',
        	),
        	array(
            	'setting_name' => 'companyDesc',
            	'setting_value' => 'A fast-growing company!',
            	'setting_desc' => 'Company Description',
        	),
            array(
                'setting_name' => 'companyAddress',
                'setting_value' => 'Tamansiswa Business Center Slot B2 Jl. Tamansiswa No. 160 Mergangsan, Yogyakarta',
                'setting_desc' => 'Company Address',
            ),
            array(
                'setting_name' => 'companyLatLon',
                'setting_value' => '-7.7955798,110.36948959999995',
                'setting_desc' => 'Company Latitude Longitude',
            ),
            array(
                'setting_name' => 'companyEmail',
                'setting_value' => 'mycompany@example.com',
                'setting_desc' => 'Company Email',
            ),
            array(
                'setting_name' => 'companyPhone',
                'setting_value' => '0123-4567-8900',
                'setting_desc' => 'Company Phone',
            ),
            array(
                'setting_name' => 'companyFacebook',
                'setting_value' => 'http://facebook.com/',
                'setting_desc' => 'Company Facebook Account',
            ),
            array(
                'setting_name' => 'companyInstagram',
                'setting_value' => 'http://instagram.com/',
                'setting_desc' => 'Company Instagram Account',
            ),
            array(
                'setting_name' => 'companyTwitter',
                'setting_value' => 'http://twitter.com/',
                'setting_desc' => 'Company Twitter Account',
            ),
            array(
                'setting_name' => 'companyPinterest',
                'setting_value' => 'http://pinterest.com/',
                'setting_desc' => 'Company Pinterest Account',
            ),
            array(
                'setting_name' => 'companyPath',
                'setting_value' => 'http://path.com/',
                'setting_desc' => 'Company Path Account',
            ),
            array(
                'setting_name' => 'companyGooglePlus',
                'setting_value' => 'http://plus.google.com/',
                'setting_desc' => 'Company Google+ Account',
            ),
            array(
                'setting_name' => 'companyLinkedIn',
                'setting_value' => 'http://linkedin.com/',
                'setting_desc' => 'Company LinkedIn Account',
            ),
            array(
                'setting_name' => 'companySnapchat',
                'setting_value' => 'http://snapchat.com/',
                'setting_desc' => 'Company Snapchat Account',
            ),
            array(
                'setting_name' => 'pageName',
                'setting_value' => 'Halaman 1|Halaman 2|Tentang Kami|Kontak',
                'setting_desc' => 'Page names',
            ),
            array(
                'setting_name' => 'publishPage',
                'setting_value' => 'publish|publish',
                'setting_desc' => 'Hide and publish page',
            ),
            )
        );
    }
}
