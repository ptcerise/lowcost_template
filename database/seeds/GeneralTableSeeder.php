<?php

use Illuminate\Database\Seeder;

class GeneralTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('general')->insert(array(
            	array(
                	'general_title' => '',
                	'general_content' => 'ATTRACTIVE SENTENCE THAT LEADS TO THE MAIN PRODUCT',
                	'general_url' => url('section1'),
                	'general_page' => 'home',
                	'general_section' => 'divider1',
                	'general_desc' => 'Divider Home #1',
            	),
            	array(
                	'general_title' => '',
                	'general_content' => 'ATTRACTIVE SENTENCE OR MOTTO',
                	'general_url' => url('about'),
                	'general_page' => 'home',
                	'general_section' => 'divider2',
                	'general_desc' => 'Divider Home #2',
            	),
              array(
                  'general_title' => 'Introduction Title',
                	'general_content' => '<p>Introduction Content. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>',
                	'general_url' => '',
                	'general_page' => 'home',
                	'general_section' => 'content',
                	'general_desc' => 'Introduction Content',
            	),
            )
        );
    }
}
