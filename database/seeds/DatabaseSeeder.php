<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(GeneralTableSeeder::class);
    	$this->call(MediaTableSeeder::class);
    	$this->call(SettingsTableSeeder::class);
    	$this->call(UserTableSeeder::class);
    }
}
