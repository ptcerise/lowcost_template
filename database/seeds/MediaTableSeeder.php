<?php

use Illuminate\Database\Seeder;

class MediaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('media')->insert(array(
        	array(
            	'media_title' => 'Title of the section 1',
            	'media_content' => '<p>A small paragraph that describes the section 1</p>',
            	'media_url' => 'banner1.jpg',
            	'media_page' => 'home',
            	'media_section' => 'banner-content1',
            	'media_desc' => 'Banner Content Home #1',
        	),
        	array(
            	'media_title' => 'Title of the section 2',
            	'media_content' => '<p>A small paragraph that describes the section 2</p>',
            	'media_url' => 'banner2.jpg',
            	'media_page' => 'home',
            	'media_section' => 'banner-content2',
            	'media_desc' => 'Banner Content Home #2',
        	),
        	array(
            	'media_title' => '',
            	'media_content' => '',
            	'media_url' => 'banner1.jpg',
            	'media_page' => 'section1',
            	'media_section' => 'banner-header',
            	'media_desc' => 'Banner Header Section1',
        	),
        	array(
            	'media_title' => '',
            	'media_content' => '',
            	'media_url' => 'banner2.jpg',
            	'media_page' => 'section2',
            	'media_section' => 'banner-header',
            	'media_desc' => 'Banner Header Section2',
        	),
        	array(
            	'media_title' => '',
            	'media_content' => '',
            	'media_url' => 'banner1.jpg',
            	'media_page' => 'about',
            	'media_section' => 'banner-header',
            	'media_desc' => 'Banner Header About',
        	),
        	array(
            	'media_title' => '',
            	'media_content' => '',
            	'media_url' => 'banner2.jpg',
            	'media_page' => 'contact',
            	'media_section' => 'banner-header',
            	'media_desc' => 'Banner Header Contact',
        	),
          array(
            	'media_title' => 'Carousel Image Example',
            	'media_content' => 'carousel-alt-tag',
            	'media_url' => 'carousel-default.jpg',
            	'media_page' => 'home',
            	'media_section' => 'carousel',
            	'media_desc' => 'Carousel Image',
              'temp_id' => 1,
        	),
            )
        );
    }
}
