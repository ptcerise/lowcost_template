<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/laravel',function(){
	return view('welcome');
});

Auth::routes();

/* -----------------FRONT END------------------ */
/** URL ROUTES */
Route::get('/', 'Home@index')->name('home');
Route::get('/home', function(){
	return redirect()->route('home');
});

$page = explode('|',strtolower(App\Settings::where(['setting_name'=>'pageName'])->first()->setting_value));
$publish = explode('|',App\Settings::where(['setting_name'=>'publishPage'])->first()->setting_value);

function clean($string) {
   $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
   $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

   return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
}

if($publish[0] == 'publish'):
	Route::get('/'.clean($page[0]), 'Sections@index')->name('section1');
else:
	Route::get('/#', 'Sections@index')->name('section1');
endif;
if($publish[1] == 'publish'):
	Route::get('/'.clean($page[1]), 'Sections@index')->name('section2');
else:
	Route::get('/#', 'Sections@index')->name('section2');
endif;
Route::get('/'.clean($page[2]), 'About@index')->name('about');
Route::get('/'.clean($page[3]), 'Contact@index')->name('contact');

/** CRUD ROUTES */
Route::post('/contact/send_message', 'Contact@sendMessage');

/* END OF FRONT END */

/* -----------------BACK END----------------- */

/** LOGIN */
// Route::get('/backend/login', 'BackendLogin@index')->name('login');
// Route::post('/backend/auth/login', 'BackendLogin@auth');
// Route::get('backend/logout', 'BackendLogin@logout')->name('logout');

Route::get('/backend',function(){
	return redirect()->route('backendHome');
});
/** DASHBOARD */
Route::get('/backend/dashboard', 'BackendDashboard@index')->name('backendDashboard');


// /** SETTINGS */
Route::get('/backend/settings', 'BackendSettings@index')->name('backendSettings');
// Route::post('/backend/settings/update', 'BackendSettings@update')->name('updateSettings');
/** PAGE SETTING **/
Route::post('/backend/settings/saveprofile','BackendSettings@saveprofile')->name('saveProfile');
Route::post('/backend/settings/savefont','BackendSettings@savefont')->name('saveFont');
Route::post('/backend/settings/save_color', 'BackendSettings@save_color');
Route::post('/backend/settings/save_page_name', 'BackendSettings@save_page_name');
Route::post('/backend/settings/page_publish', 'BackendSettings@page_publish');

/** USER */
Route::get('/backend/user_management', 'BackendUser@index')->name('backendUser');
Route::get('/backend/user_management/update/{id}', 'BackendUser@update');

Route::post('/backend/user_management/register', 'BackendUser@register')->name('addUser');
Route::post('/backend/user_management/saveupdate', 'BackendUser@saveupdate')->name('updateUser');
Route::post('/backend/user_management/level', 'BackendUser@level')->name('levelUser');
Route::delete('/backend/user_management/delete/{id}', 'BackendUser@delete')->name('deleteUser');

/** HOME */
Route::get('/backend/home','BackendHome@index')->name('backendHome');
Route::post('/backend/home/save_carousel','BackendHome@save_carousel');
Route::post('/backend/home/save_content','BackendHome@save_content');
Route::delete('/backend/home/delete_carousel/{id}', 'BackendHome@delete_carousel');
Route::post('/backend/home/order_carousel','BackendHome@order_carousel');

Route::post('/backend/home/save_divider','BackendHome@save_divider');
Route::post('/backend/home/save_banner','BackendHome@save_banner');

Route::post('/backend/home/save_card', 'BackendHome@save_card');
Route::post('/backend/home/order_card', 'BackendHome@order_card');
Route::delete('/backend/home/delete_card/{id}', 'BackendHome@delete_card');

/** SECTIONS */
Route::get('/backend/sections/{section}','BackendSections@index')->name('backendSections');
Route::post('/backend/sections/save_banner','BackendSections@save_banner');
Route::post('/backend/sections/save_content','BackendSections@save_content');
Route::post('/backend/sections/update_content/{id}','BackendSections@save_content');
Route::delete('/backend/sections/delete_content/{id}','BackendSections@delete_content');
Route::post('/backend/sections/order_content','BackendSections@order_content');

/** ABOUT */
Route::get('/backend/about','BackendAbout@index')->name('backendAbout');
Route::post('/backend/about/save_banner','BackendAbout@save_banner');
Route::post('/backend/about/save_content','BackendAbout@save_content');
Route::post('/backend/about/update_content/{id}','BackendAbout@save_content');
Route::delete('/backend/about/delete_content/{id}','BackendAbout@delete_content');
Route::post('/backend/about/order_content','BackendAbout@order_content');

/** CONTACT */
Route::get('/backend/contact','BackendContact@index')->name('backendContact');
Route::post('/backend/contact/savecontactinfo','BackendContact@savecontactinfo')->name('saveContactInfo');
Route::post('/backend/contact/savesocmed','BackendContact@savesocmed')->name('saveSocmed');
Route::post('/backend/contact/save_banner','BackendContact@save_banner');
Route::post('/backend/contact/read_message','BackendContact@read_message');
Route::delete('/backend/contact/delete_message/{id}','BackendContact@delete_message');
